# -*- coding: utf-8 -*-

##
# Created on Tue Nov 13 15:52:48 2018
##

import os


class JobOrder:
    def __init__(self, AuxInputList, ParentFolder):
        fields = {}

        if os.path.exists(AuxInputList):
            with open(AuxInputList, 'r') as auxFile:
                next(auxFile)
                for line in auxFile:
                    key, value = line.split("=")
                    fields[key] = value.rstrip("\n")
        else:
            print(" Error missing aux input list file : ", AuxInputList)
            return

        try:
            self.InputL3TBlist = ParentFolder + fields["InputL3TBlist"]
            self.InputL3ECMWFlist = ParentFolder + fields["InputL3ECMWFlist"]
            self.OutputFolderName = ParentFolder + fields["OutputFolderName"]
            self.LandCoverFractionsFileName = ParentFolder + fields["LandCoverFractionsFileName"]
            self.LandCoverClassesParameterIGBPxlsFileName = ParentFolder + fields["LandCoverClassesParameterIGBPxlsFileName"]
            self.SoilPropertiesFileName = ParentFolder + fields["SoilPropertiesFileName"]
            self.InitalTauFileName = ParentFolder + fields["InitalTauFileName"]
            self.CurrentTauFileName = ParentFolder + fields["CurrentTauFileName"]
            self.TopographyFileName = ParentFolder + fields["TopographyFileName"]
            self.LandSeaMaskFileName = ParentFolder + fields["LandSeaMaskFileName"]
            self.AreaOfInterestCoordinatesFileName = ParentFolder + fields["AreaOfInterestCoordinatesFileName"]
            self.Version = fields["Version"]
        except KeyError as e:
            print('Coordinate file: {} is of incorrect format or contains invalid values.'.format(AuxInputList))
            return
