# -*- coding: utf-8 -*-

# Created on Wed Nov 21 23:07:00 2018
# @author: Fred

import os
import csv
import numpy as np
from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager

class AuxLandCover(SharedArrayManager):
    def __init__(self, fileName, csvFileLcc):
        super().__init__()

        h_igbp = []
        nrh = []
        nrv = []
        albedo_igbp = []
        lccParam = []

        if os.path.exists(csvFileLcc):
            with open(csvFileLcc, mode="r", encoding="utf-8") as f:
                csv_f = csv.reader(f, delimiter=";")
                next(csv_f) # to skip header
                for row in csv_f:
                    alcc = row[2][-1]
                    h_igbp.append(row[3])
                    nrh.append(row[6])
                    nrv.append(row[7])
                    albedo_igbp.append(row[14])
                    lccParam.append([alcc] + row[3:20])
        else:
            print("Error : file not found", csvFileLcc)
            return

        h_igbp = np.array(h_igbp, dtype=np.float)
        nrh = np.array(nrh, dtype=np.int)
        nrv = np.array(nrv, dtype=np.int)
        albedo_igbp = np.array(albedo_igbp, dtype=np.float)
        self.lccParam = np.array(lccParam, dtype=np.float)

        if os.path.exists(fileName):
            dataFile = netcdf_file(fileName, 'r')
            lcFractions = dataFile.variables['EASE2IGBPFractions'][:]
            axes = (0, 0)

            self.H_igbpGlobal = SharedArrayManager.copy_array(np.tensordot(lcFractions, h_igbp, axes))
            self.nrhGlobal = SharedArrayManager.copy_array(np.tensordot(lcFractions, nrh, axes))
            self.nrvGlobal = SharedArrayManager.copy_array(np.tensordot(lcFractions, nrv, axes))
            self.albedo_igbp_Global = SharedArrayManager.copy_array(np.tensordot(lcFractions, albedo_igbp, axes))
            WetUrIceInd = [0, 11, 13, 15] # water, wetland, urban, snow, ice
            self.WetUrIce = SharedArrayManager.copy_array(lcFractions[WetUrIceInd].sum(axis=0))

            del lcFractions
            dataFile.close()
