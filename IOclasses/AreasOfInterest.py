# -*- coding: utf-8 -*-

from scipy.io.netcdf import netcdf_file
import numpy as np
import os


class AreasOfInterest:
    def __init__(self, landSeaFile, coordinateFile):
        # CM : variables globales à la fonction: sorties du try
        self.region = []
        self.select = []
        self.latMin = []
        self.latMax = []
        self.lonMin = []
        self.lonMax = []
        self.rows = np.empty(0, dtype=np.int)
        self.cols = np.empty(0, dtype=np.int)

        try:
            if os.path.exists(coordinateFile):
                with open(coordinateFile, "r") as coordFile:
                    next(coordFile)
                    for line in coordFile:
                        fields = line.split(",")
                        self.region.append(fields[0])
                        self.select.append(int(fields[1]))
                        self.latMin.append(float(fields[2]))
                        self.latMax.append(float(fields[3]))
                        self.lonMin.append(float(fields[4]))
                        self.lonMax.append(float(fields[5]))
                    # vérifier que le nb de champs attendu est correct ?
                    # si absence de , => 2 champs dans 1 et le dernier vide
            else:
                print("Error missing coordinate file", coordinateFile)
                return
        except Exception as e:
            print('Coordinate file {} is of incorrect format or contains invalid values.'.format(coordinateFile), e)
            return

        ncfile = netcdf_file(landSeaFile, "r")
        lat = ncfile.variables['lat'][:]
        lon = ncfile.variables['lon'][:]
        selected = np.zeros((lat.size, lon.size), dtype=np.bool)

        for i in range(len(self.select)):
            if self.select[i] == 1:
                i_lat_min = np.searchsorted(lat, self.latMin[i])
                i_lat_max = np.searchsorted(lat, self.latMax[i])
                i_lon_min = np.searchsorted(lon, self.lonMin[i])
                i_lon_max = np.searchsorted(lon, self.lonMax[i])
                selected[i_lat_min:i_lat_max,i_lon_min:i_lon_max] = True

        selected &= ncfile.variables["USGS_Land_Flag"][:].view(np.bool)
        self.rows, self.cols = selected.nonzero()

        del lat, lon
        ncfile.close()

    def __len__(self):
        return len(self.rows)
