# -*- coding: utf-8 -*-
# Created on Tue Nov 27 15:54:09 2018
# @author: Fred

from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager


class AuxTopography(SharedArrayManager):
    def __init__(self, fileName):
        super().__init__()

        ncfile = netcdf_file(fileName, 'r')
        self.strongTopography = SharedArrayManager.copy_array(ncfile.variables['strongTopography'][:])
        self.strongTopography /= 100
        self.moderateTopography = SharedArrayManager.copy_array(ncfile.variables['moderateTopography'][:])
        self.moderateTopography /= 100
        ncfile.close()
