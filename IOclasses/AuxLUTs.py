# -*- coding: utf-8 -*-

from .AuxLandCover import AuxLandCover
from .AuxSoilProperty import AuxSoilProperty
from .AuxInitTauNad import AuxInitTauNad
from .AuxTopography import AuxTopography
from .AreasOfInterest import AreasOfInterest
from UserParameters.ProcessingParameters import ProcessingParameters
from UserParameters.upfConstants import upfConstants
import UserParameters.Constants

class AuxLUTs:
    def __init__(self, auxFileNames):
        self.landCoverPars = AuxLandCover(auxFileNames.LandCoverFractionsFileName, auxFileNames.LandCoverClassesParameterIGBPxlsFileName)
        self.soilProperties = AuxSoilProperty(auxFileNames.SoilPropertiesFileName)
        self.tau = AuxInitTauNad(auxFileNames.InitalTauFileName, auxFileNames.CurrentTauFileName)
        self.topography = AuxTopography(auxFileNames.TopographyFileName)

        listConst = [UserParameters.Constants.DielectricmodelConstants,
                     UserParameters.Constants.DielectricwatermodelConstants,
                     UserParameters.Constants.EffectivetemperaturemodelConstants,
                     UserParameters.Constants.AtmospheremodelConstants,
                     UserParameters.Constants.DobsonmodelConstants,
                     UserParameters.Constants.SkymodelConstants,
                     UserParameters.Constants.AllmodelsConstants,
                     UserParameters.Constants.SurfaceroughnessConstants,
                     UserParameters.Constants.PreprocessingConstants,
                     UserParameters.Constants.PostprocessingConstants,
                     UserParameters.Constants.PhysicalConstants,
                     UserParameters.Constants.FresnelmodelConstants,
                     UserParameters.Constants.SMOSConstants,
                     UserParameters.Constants.MironovmodelConstants,
                     UserParameters.Constants.VegetationmodelConstants
                    ]

        self.upf = upfConstants(listConst)

        self.defaultPar = ProcessingParameters(self.landCoverPars.lccParam, self.upf)


class LCCparams:
    def __init__(self, landCover, soilProperty, topography, colsj, rowsj):
        self.albedo = landCover.albedo_igbp_Global[rowsj, colsj]
        self.roughness = landCover.H_igbpGlobal[rowsj, colsj]
        self.clayFraction = soilProperty.clayDataEASE2[rowsj, colsj]
        self.topoModerate = topography.moderateTopography[rowsj, colsj]
        self.topoStrong = topography.strongTopography[rowsj, colsj]
        self.WetUrbanIceFraction = landCover.WetUrIce[rowsj, colsj]
        self.nrh = landCover.nrhGlobal[rowsj, colsj]
        self.nrv = landCover.nrvGlobal[rowsj, colsj]

    def getSceneFlags(self, upf, ecmwf):
        sceneFlag = 0
        exitFlag = 0
        if self.topoModerate > upf.th_scene_ftm[0]:
            sceneFlag = 1  # presence of moderate topography
        if self.topoStrong > upf.th_scene_fts[0]:
            sceneFlag = sceneFlag | 2  # presence of strong topography
        if self.WetUrbanIceFraction > upf.th_scene_wetUrIce[0]:
            sceneFlag  = sceneFlag | 4  # polluted scene (too much water, urban, ice)
            exitFlag = 1
        if ecmwf.Soil_Temperature_L1 < upf.zero_c_in_k:
            sceneFlag = sceneFlag | 8   # frozen scene
            exitFlag = 2

        return exitFlag, sceneFlag

