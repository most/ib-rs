# -*- coding: utf-8 -*-

import numpy as np
from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager


class ECMWFTemperatures:
    def __init__(self, Soil_Temperature_L1, Soil_Temperature_L3, Air_Temperature_2m, Skin_Temperature):
        self.Soil_Temperature_L1 = Soil_Temperature_L1
        self.Soil_Temperature_L3 = Soil_Temperature_L3
        self.Air_Temperature_2m = Air_Temperature_2m
        self.Skin_Temperature = Skin_Temperature


class AuxECMWF(SharedArrayManager):
    def __init__(self, fileName):
        super().__init__()

        ncfile = netcdf_file(fileName, 'r')

        var = ncfile.variables["Air_Temperature_2m"]
        self.Air_Temperature_2m = SharedArrayManager.copy_array(var[:])
        self.Air_Temperature_2m[self.Air_Temperature_2m == var._FillValue] = np.nan

        # self.Surface_Pressure = ncfile.variables['Surface_Pressure'][:].copy()
        # self.Surface_Pressure[self.Surface_Pressure == ncfile.variables['Surface_Pressure']._FillValue ] = np.nan

        # self.Total_Column_Water_Vapor = ncfile.variables['Total_Column_Water_Vapor'][:].copy()
        # self.Total_Column_Water_Vapor[self.Total_Column_Water_Vapor == ncfile.variables['Total_Column_Water_Vapor']._FillValue ] = np.nan

        var = ncfile.variables["Skin_Temperature"]
        self.Skin_Temperature = SharedArrayManager.copy_array(var[:])
        self.Skin_Temperature[self.Skin_Temperature == var._FillValue] = np.nan

        # self.Scaled_Volumetric_Soil_Water_L1 = ncfile.variables['Scaled_Volumetric_Soil_Water_L1'][:].copy()
        # self.Scaled_Volumetric_Soil_Water_L1[self.Scaled_Volumetric_Soil_Water_L1 == ncfile.variables['Scaled_Volumetric_Soil_Water_L1']._FillValue ] = np.nan

        # self.Volumetric_Soil_Water = ncfile.variables['Volumetric_Soil_Water'][:].copy()
        # self.Volumetric_Soil_Water[self.Volumetric_Soil_Water == ncfile.variables['Volumetric_Soil_Water']._FillValue ] = np.nan

        var = ncfile.variables["Soil_Temperature"]
        self.Soil_Temperature_L1 = SharedArrayManager.copy_array(var[..., 0])
        self.Soil_Temperature_L1[self.Soil_Temperature_L1 == var._FillValue] = np.nan

        self.Soil_Temperature_L3 = SharedArrayManager.copy_array(var[..., 2])
        self.Soil_Temperature_L3[self.Soil_Temperature_L3 == var._FillValue] = np.nan

        del var
        ncfile.close()

    def getTemps(self, rowsj, colsj):
        return ECMWFTemperatures(self.Soil_Temperature_L1[rowsj, colsj], self.Soil_Temperature_L3[rowsj, colsj],
                                 self.Air_Temperature_2m[rowsj, colsj], self.Skin_Temperature[rowsj, colsj])
