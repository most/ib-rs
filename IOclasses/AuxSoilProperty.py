# -*- coding: utf-8 -*-
# Created on Tue Nov 27 15:10:01 2018
# @author: Fred

from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager


class AuxSoilProperty(SharedArrayManager):
    def __init__(self, fileName):
        super().__init__()

        ncfile = netcdf_file(fileName, 'r')
        self.clayDataEASE2 = SharedArrayManager.copy_array(ncfile.variables["clayEASE2"][:])
        ncfile.close()
