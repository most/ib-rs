# -*- coding: utf-8 -*-

import numpy as np
from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager


class L3SMProduct(SharedArrayManager):
    def __init__(self, cdf3tb, ECM):
        super().__init__()

        self.lat = cdf3tb.lat
        self.lon = cdf3tb.lon
        shape = (len(self.lat), len(self.lon))

        self.Soil_Moisture = SharedArrayManager.new_array(shape, np.float, np.nan)
        self.Soil_Moisture_StdError = SharedArrayManager.new_array(shape, np.float, np.nan)
        self.Optical_Thickness_Nad = SharedArrayManager.new_array(shape, np.float, np.nan)
        self.Optical_Thickness_Nad_StdError = SharedArrayManager.new_array(shape, np.float, np.nan)

        self.Scene_Flags = SharedArrayManager.new_array(shape, np.int8, -128)
        self.RMSE = SharedArrayManager.new_array(shape, np.float, np.nan)
        self.Skin_Temperature_ECMWF = ECM.Skin_Temperature
        self.Soil_Temperature_Level1 = ECM.Soil_Temperature_L1
        self.Days = SharedArrayManager.new_array(shape, np.int32, -2147483647)
        self.UTC_Seconds = SharedArrayManager.new_array(shape, np.int32, -2147483647)
        self.UTC_Microseconds = SharedArrayManager.new_array(shape, np.int32, -2147483647)

    def updateMetadata(self, nc, version="light"):
        """
        add metadata information
        """
        newReference = "Wigneron et al., SMOS-IC data record of soil moisture and L-VOD: historical development, applications and perspectives, Remote Sens. Env., 254, 112238, 2021, https://doi.org/10.1016/j.rse.2020.112238"
        newInstitution = "INRAE BORDEAUX"
        contact = "jean-pierre.wigneron@inrae.fr"
        # replace reference
        # replace Institution
        # version =
        # rootgrp = Dataset(fileName, "w", format="NETCDF4")
        # print("\t attributes : ", dir(nc))
        
        # doivent être définis comme attributs
        # nc.history = " test history"
        nc.reference = newReference
        nc.institution = newInstitution
        nc.contact = contact
        nc.version = version
        """
        nc.createVariable('reference', 'S1', [])
        nc.createVariable('version', 'S1', [])
        nc.createVariable('institution', 'S1', [])
        nc.createVariable('contact', 'S1', [])
#        ref = newReference
#        inst = newInstitution
#        ver = version
#        cont = contact
        """
        """        
        nc.createVariable('information', 'S1', [])
        nc.variables['information'].reference = newReference
        nc.variables['information'].institution = newInstitution
        nc.variables['information'].version = version
        nc.variables['information'].contact = contact
        """
        """
        # pb ci-dessous
        nc.variables['reference'] = newReference
        
        nc.variables['institution'] = newInstitution
        nc.variables['version'] = version
        nc.variables['contact'] = contact
        """
        # rootgrp.description = "SMOS-IC v2.0"

        # nc.createVariable('crs', 'S1', [])

        # print("\n metadata : TBD \n")

    def generateSmProduct(self, fileName, version="light"):
        nc = netcdf_file(fileName, 'w')
        self.generateProduct_start(nc)
        nc.createVariable('Soil_Moisture', np.float32, ['lat','lon'])
        nc.createVariable('Soil_Moisture_StdError', np.float32, ['lat','lon'])

        self.generateProduct_end(nc, version)
        nc.close()

    def generateSmVodProduct(self, fileName, version="light"):
        nc = netcdf_file(fileName, 'w')

        self.generateProduct_start(nc)
        nc.createVariable('Soil_Moisture', np.float32, ['lat','lon'])
        nc.createVariable('Optical_Thickness_Nad', np.float32, ['lat','lon'])
        nc.createVariable('Soil_Moisture_StdError', np.float32, ['lat','lon'])
        nc.createVariable('Optical_Thickness_Nad_StdError', np.float32, ['lat','lon'])

        nc.variables['Optical_Thickness_Nad'][:] = self.Optical_Thickness_Nad
        nc.variables['Optical_Thickness_Nad'].grid_mapping = 'crs'
        nc.variables['Optical_Thickness_Nad_StdError'][:] = self.Optical_Thickness_Nad_StdError
        nc.variables['Optical_Thickness_Nad_StdError'].grid_mapping = 'crs'

        self.generateProduct_end(nc, version)

#         self.generateProduct(nc, version)

        nc.close()

    def generateProduct(self, nc, version="light"):
        # nc = netcdf_file(fileName, 'w')

        nc.createDimension('lat', len(self.lat))
        nc.createDimension('lon', len(self.lon))

        nc.createVariable('crs', 'S1', [])
        nc.createVariable('lon', np.float32, ['lon'])
        nc.createVariable('lat', np.float32, ['lat'])
        nc.createVariable('Soil_Moisture', np.float32, ['lat','lon'])
#        nc.createVariable('Optical_Thickness_Nad', np.float32, ['lat','lon'])
        nc.createVariable('Soil_Moisture_StdError', np.float32, ['lat','lon'])
#        nc.createVariable('Optical_Thickness_Nad_StdError', np.float32, ['lat','lon'])

        nc.createVariable('Scene_Flags', np.int8, ['lat','lon'])
        nc.createVariable('RMSE', np.float32, ['lat','lon'])
        nc.createVariable('Skin_Temperature_ECMWF', np.float32, ['lat', 'lon'])
        nc.createVariable('Soil_Temperature_Level1', np.float32, ['lat','lon'])
        nc.createVariable('Days', np.int32, ['lat','lon'])
        nc.createVariable('UTC_Seconds', np.int32, ['lat','lon'])
        nc.createVariable('UTC_Microseconds', np.int32, ['lat','lon'])

        nc.variables['crs'].spatial_ref = 'PROJCS["EASE-Grid 2.0",'\
                                              'GEOGCS["WGS 84",'\
                                                  'DATUM["WGS_1984",'\
                                                      'SPHEROID["WGS 84",6378137,298.257223563]],'\
                                                  'PRIMEM["Greenwich",0],'\
                                                  'UNIT["Degree",0.0174532925199433]],'\
                                              'PROJECTION["Cylindrical_Equal_Area"],'\
                                              'PARAMETER["standard_parallel_1",30],'\
                                              'PARAMETER["central_meridian",0],'\
                                              'PARAMETER["false_easting",0],'\
                                              'PARAMETER["false_northing",0],'\
                                              'UNIT["Meter",1]]'
        nc.variables['crs'].GeoTransform = '-17367530.44 25025.26 0 7307375.92 0 -25025.26'
        nc.variables['lon'][:] = self.lon
        nc.variables['lat'][:] = self.lat
        nc.variables['Soil_Moisture'][:] = self.Soil_Moisture
        nc.variables['Soil_Moisture'].grid_mapping = 'crs'
#        nc.variables['Optical_Thickness_Nad'][:] = self.Optical_Thickness_Nad
#        nc.variables['Optical_Thickness_Nad'].grid_mapping = 'crs'
#        nc.variables['Soil_Moisture_StdError'][:] = self.Soil_Moisture_StdError
        nc.variables['Soil_Moisture_StdError'].grid_mapping = 'crs'
#        nc.variables['Optical_Thickness_Nad_StdError'][:] = self.Optical_Thickness_Nad_StdError
#        nc.variables['Optical_Thickness_Nad_StdError'].grid_mapping = 'crs'

        nc.variables['Scene_Flags'][:] = self.Scene_Flags
        nc.variables['Scene_Flags'].grid_mapping = 'crs'
        nc.variables['RMSE'][:] = self.RMSE
        nc.variables['RMSE'].grid_mapping = 'crs'
        nc.variables['Skin_Temperature_ECMWF'][:] = self.Skin_Temperature_ECMWF
        nc.variables['Skin_Temperature_ECMWF'].grid_mapping = 'crs'
        nc.variables['Soil_Temperature_Level1'][:] = self.Soil_Temperature_Level1
        nc.variables['Soil_Temperature_Level1'].grid_mapping = 'crs'
        nc.variables['Days'][:] = self.Days
        nc.variables['Days'].grid_mapping = 'crs'
        nc.variables['UTC_Seconds'][:] = self.UTC_Seconds
        nc.variables['UTC_Seconds'].grid_mapping = 'crs'
        nc.variables['UTC_Microseconds'][:] = self.UTC_Microseconds
        nc.variables['UTC_Microseconds'].grid_mapping = 'crs'

        self.updateMetadata(nc, version)

        # nc.close()

    def generateProduct_start(self, nc):
        # nc = netcdf_file(fileName, 'w')

        nc.createDimension('lat', len(self.lat))
        nc.createDimension('lon', len(self.lon))

        nc.createVariable('crs', 'S1', [])
        nc.createVariable('lon', np.float32, ['lon'])
        nc.createVariable('lat', np.float32, ['lat'])

    def generateProduct_end(self, nc, version="light"):
        # nc = netcdf_file(fileName, 'w')

        nc.createVariable('Scene_Flags', np.int8, ['lat','lon'])
        nc.createVariable('RMSE', np.float32, ['lat','lon'])
        nc.createVariable('Skin_Temperature_ECMWF', np.float32, ['lat', 'lon'])
        nc.createVariable('Soil_Temperature_Level1', np.float32, ['lat','lon'])
        nc.createVariable('Days', np.int32, ['lat','lon'])
        nc.createVariable('UTC_Seconds', np.int32, ['lat','lon'])
        nc.createVariable('UTC_Microseconds', np.int32, ['lat','lon'])

        nc.variables['crs'].spatial_ref = 'PROJCS["EASE-Grid 2.0",'\
                                              'GEOGCS["WGS 84",'\
                                                  'DATUM["WGS_1984",'\
                                                      'SPHEROID["WGS 84",6378137,298.257223563]],'\
                                                  'PRIMEM["Greenwich",0],'\
                                                  'UNIT["Degree",0.0174532925199433]],'\
                                              'PROJECTION["Cylindrical_Equal_Area"],'\
                                              'PARAMETER["standard_parallel_1",30],'\
                                              'PARAMETER["central_meridian",0],'\
                                              'PARAMETER["false_easting",0],'\
                                              'PARAMETER["false_northing",0],'\
                                              'UNIT["Meter",1]]'
        nc.variables['crs'].GeoTransform = '-17367530.44 25025.26 0 7307375.92 0 -25025.26'
        nc.variables['lon'][:] = self.lon
        nc.variables['lat'][:] = self.lat
        nc.variables['Soil_Moisture'][:] = self.Soil_Moisture
        nc.variables['Soil_Moisture'].grid_mapping = 'crs'
#        nc.variables['Optical_Thickness_Nad'][:] = self.Optical_Thickness_Nad
#        nc.variables['Optical_Thickness_Nad'].grid_mapping = 'crs'
#        nc.variables['Soil_Moisture_StdError'][:] = self.Soil_Moisture_StdError
        nc.variables['Soil_Moisture_StdError'].grid_mapping = 'crs'
#        nc.variables['Optical_Thickness_Nad_StdError'][:] = self.Optical_Thickness_Nad_StdError
#        nc.variables['Optical_Thickness_Nad_StdError'].grid_mapping = 'crs'

        nc.variables['Scene_Flags'][:] = self.Scene_Flags
        nc.variables['Scene_Flags'].grid_mapping = 'crs'
        nc.variables['RMSE'][:] = self.RMSE
        nc.variables['RMSE'].grid_mapping = 'crs'
        nc.variables['Skin_Temperature_ECMWF'][:] = self.Skin_Temperature_ECMWF
        nc.variables['Skin_Temperature_ECMWF'].grid_mapping = 'crs'
        nc.variables['Soil_Temperature_Level1'][:] = self.Soil_Temperature_Level1
        nc.variables['Soil_Temperature_Level1'].grid_mapping = 'crs'
        nc.variables['Days'][:] = self.Days
        nc.variables['Days'].grid_mapping = 'crs'
        nc.variables['UTC_Seconds'][:] = self.UTC_Seconds
        nc.variables['UTC_Seconds'].grid_mapping = 'crs'
        nc.variables['UTC_Microseconds'][:] = self.UTC_Microseconds
        nc.variables['UTC_Microseconds'].grid_mapping = 'crs'

        self.updateMetadata(nc, version)

