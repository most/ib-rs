# -*- coding: utf-8 -*-
# Created on Tue Nov 27 16:57:18 2018
# @author: Fred

import warnings
import numpy as np
from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager

class AuxInitTauNad(SharedArrayManager):
    def __init__(self, initFixedTau, currentTau):
        super().__init__()

        ncfile = netcdf_file(initFixedTau, 'r')
        self.monthlyTauNad = SharedArrayManager.copy_array(ncfile.variables["each_month_median_vod"][:])
        ncfile.close()
        # ncfile = netcdf_file(currentTau, 'r')
        # self.currentTauNad = ncfile.variables['currentTauNad'][:].copy()
        # ncfile.close()

    def set_init_tau(self, window, month, upf):
        """
        Compute mean of previous VODs, replacing invalid values with generic ones
        """
        total = np.zeros(self.monthlyTauNad.shape[1:], dtype=np.float32)
        count = np.zeros(total.shape, dtype=np.uint8)

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=RuntimeWarning)

            for filename in window:
                ncfile = netcdf_file(filename, "r")

                # because of NaN values, this is NOT equivalent to ~(RMSE > max)
                valid = (ncfile.variables["RMSE"][:] <= upf.th_max_rmse[0])
                count += valid
                total[valid] += ncfile.variables["Optical_Thickness_Nad"][valid]

                ncfile.close()

            total[count == 0] = np.nan
            total /= count
            # because of NaN values, this is NOT equivalent to ((total < 0.0) | (total > 2.0))
            invalid = ~((total >= upf.taus_nad_min) & (total <= upf.taus_nad_max))

        np.copyto(total, self.monthlyTauNad[month - 1], where=invalid)

        self.initialTauNad = SharedArrayManager.copy_array(total)
