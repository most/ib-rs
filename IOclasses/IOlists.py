# -*- coding: utf-8 -*-

# Created on Wed Nov 21 16:26:29 2018
# @author: Fred
# CM : remarque : code pour désigner le site de Villenave ?

import os
import re
import datetime


class IOlists:
    def __init__(self, L3rTbFileName, ecmwfFileName, smOutputFolderName, DebugMode, VersionProc):
        # liste des patterns pour les noms de fichier

        # description du nom de fichier SMOS :
        #     SM_RE06_MIR_CDF3TA_20181230T000000_20181230T235959_315_001_7.DBL
        #     SM              : produit Soil Moisture / SMOS
        #     RE06            : Reprocessed version 6
        #     MIR             : Mironov
        #     CDF3TA          :
        #                         * C : Catds
        #                         * D : Daily product
        #                         * F : Full polarization
        #                         * 3 : level 3
        #                         * T : TB
        #                         * A : orbite (Ascending)
        #     20181230T000000 : date/heure de début  YYYYMMDD T HHMMSS
        #     20181230T235959 : date/heure de fin
        #     315             : version du produit L3
        #     001             : compteur de fichier
        #     7               : site de traitement

        # *) Product Version = V.105
        # *) Naming convention
        # example :
        #     SM_OPER_MIR_CDF3SA_20160726T000000_20160726T235959_105_001_8.DBL.nc
        #     SM : SMOS
        #     OPER : OPER or REXX, depends if SMOS L3 brightness temperatures are
        #     from the operationnal or reprocessing campaign
        #     MIR : MIRAS SMOS instrument
        #     CDF3SA/CDF3SD : Catds Daily, Full polarization, level 3, Smosic,
        #                     Ascending/Descending orbits
        #     20160726 : Tag for the day as 4 digit year, 2 digit month, 2 digit day
        #     000000 & 235959 : time window, as HHMNSS, means all orbits of the day
        #         are considered
        #     105 : version of the SMOS IC product
        #     001 : file counter
        #     8 : processing site, 8 stands for Cesbio
        #     DBL.nc : DataBLock, nc is the extension for netcdf format

        # SM_RE06_MIR_CDF3TA_20181230T000000_20181230T235959_315_001_7.DBL
        
        # tbPattern 
        # L3TB files vide : pb RE, pb fin .DBL.nc ?  pourtant pas de $ en fin de RE
        # ECM OK 
        
        # tbPattern = re.compile(r"SM_\w{4}_MIR_CDF3T(A|D)_\d{8}T\d{6}_\d{8}T\d{6}_\d{3}_\d{3}_\d\.DBL")
        tbPattern = re.compile(r"SM_\w{4}_MIR_CDF3T(A|D)_\d{8}T\d{6}_\d{8}T\d{6}_\d{3}_\d{3}_\d\.DBL(?:\.nc)?")  # optional end
        
        # SM_RE04_AUX_CDFECA_20120417T000000_20120417T235959_300_001_7.DBL.nc
        # SM_OPER_AUX_CDFECA_20200619T000000_20200619T235959_319_001_7.DBL.nc
        ecmPattern = re.compile(r"SM_\w{4}_AUX_CDFEC(A|D)_\d{8}T\d{6}_\d{8}T\d{6}_\d{3}_\d{3}_\d\.DBL(?:\.nc)?")

        get_date = lambda x: datetime.datetime.strptime(os.path.basename(x)[19:27], "%Y%m%d").date()
        matches_pattern = lambda x: lambda y: x.fullmatch(os.path.basename(y)) is not None
        # fullmatch : exact matching : does not work if .nc is added

        with open(L3rTbFileName, 'r') as l3FileList:
            
            tbflistRaw = l3FileList.read().splitlines()
            # print('\t L3TB files src : {}'.format(tbflistRaw))
            tbflistRaw = list(filter(matches_pattern(tbPattern), tbflistRaw))
            # print("\t", filter(matches_pattern(tbPattern), tbflistRaw))
            # print('\t L3TB files after filter : {}'.format(tbflistRaw))
            tbflistRaw = list(zip(map(get_date, tbflistRaw), tbflistRaw))
            tbflistRaw.sort()
            # print('\t L3TB files: {}'.format(tbflistRaw))
            print('\t L3TB files found : ', len(tbflistRaw))
            
            """
            tbflistRaws = l3FileList.read().splitlines()
            tbflistRaw = list(filter(matches_pattern(tbPatternTer),
                                        tbflistRawBis))
            tbflistRawBis = list(zip(map(get_date, tbflistRawBis),
                                     tbflistRawBis))
            tbflistRawBis.sort()
            print('\t L3TB files Bis: {}'.format(tbflistRawBis))
            # for fileName in tbflistRawBis

            # merge both list
            # mergedList = tbflistRaw + tbflistRawBis
            # mergedList.sort()
            # tbflistRaw = mergedList
            tbflistRaw = tbflistRawBis
            """
        with open(ecmwfFileName, 'r') as ecmwfFileList:
            ecmwfflistRaw = ecmwfFileList.read().splitlines()
            # print('\t ECMWF files src : {}'.format(ecmwfflistRaw))
            ecmwfflistRaw = list(filter(matches_pattern(ecmPattern),
                                        ecmwfflistRaw))
            ecmwfflistRaw = list(zip(map(get_date, ecmwfflistRaw),
                                     ecmwfflistRaw))
            ecmwfflistRaw.sort()
            # print('\t ECMWF files: {}'.format(ecmwfflistRaw))
            print('\t ECMWF files found : ', len(ecmwfflistRaw))
        self.dates = []
        self.tbflist = []
        self.ecmwfflist = []

        # intersection of sorted arrays
        i, j = 0, 0
        while i < len(tbflistRaw) and j < len(ecmwfflistRaw):
            if tbflistRaw[i][0] == ecmwfflistRaw[j][0]:
                self.dates.append(tbflistRaw[i][0])
                self.tbflist.append(tbflistRaw[i][1])
                self.ecmwfflist.append(ecmwfflistRaw[j][1])
                i += 1
                j += 1
            elif tbflistRaw[i][0] < ecmwfflistRaw[j][0]:
                print("\t no ECMWF file for file", tbflistRaw[i][1])
                i += 1
            else:
                j += 1

        # liste des fichiers SM en sortie
        self.smflist = []

        for tbf in self.tbflist:
            basename = os.path.basename(tbf)
            campaign = basename[3:7]
            orbit = basename[17:18]
            date = basename[19:50]
            fileCounter = basename[55:58]
            # newProcSite = "8"  # 9 pour l'INRA ?
            newProcSite = "B"  # B INRAE BORDEAUX
            smFileName = "SM_" + campaign + "_" + "MIR_CDF3S" + orbit + "_" + date + "_" \
                       + VersionProc + "_" + fileCounter + "_" + newProcSite + ".DBL.nc"

            if DebugMode == 1:
                smFileName = smFileName + '.DEBUG'

            smFileFullPath = os.path.join(smOutputFolderName, smFileName)
            self.smflist.append(smFileFullPath)

            if os.path.exists(smFileFullPath):
                print('\t File: {} exists and will be overwritten!'.format(smFileFullPath))

        print('\t Output products will be written to: {}'.format(smOutputFolderName))
        if not os.path.exists(smOutputFolderName):
            print('\t {} does not exist, creating the folder ...'.format(smOutputFolderName))
            os.makedirs(smOutputFolderName)

    def get_window(self, i, window_size):
        l = []

        if i < window_size:
            return l

        min_date = self.dates[i] - datetime.timedelta(window_size)
        i -= 1

        while i >= 0 and self.dates[i] >= min_date:
            l.append(self.smflist[i])
            i -= 1

        return l

    def __len__(self):
        return len(self.tbflist)

    def __getitem__(self, i):
        if i >= len(self.tbflist):
            raise IndexError
        return self.tbflist[i], self.ecmwfflist[i], self.smflist[i]
