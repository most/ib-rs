# -*- coding: utf-8 -*-

import numpy as np
from scipy.io.netcdf import netcdf_file
from Utilities.tools import SharedArrayManager


class L3TBProduct(SharedArrayManager):
    def __init__(self, fileName):
        super().__init__()

        ncfile = netcdf_file(fileName, 'r', 2)

        var = ncfile.variables["lat"]
        self.lat = SharedArrayManager.copy_array(var[:])
        self.lat[self.lat == var._FillValue] = np.nan

        var = ncfile.variables["lon"]
        self.lon = SharedArrayManager.copy_array(var[:])
        self.lon[self.lon == var._FillValue] = np.nan

        var = ncfile.variables["Grid_Point_Mask"]
        self.Grid_Point_Mask = SharedArrayManager.copy_array(var[:], np.float32)
        self.Grid_Point_Mask[self.Grid_Point_Mask == var._FillValue] = np.nan

        var = ncfile.variables["BT_H"]
        self.BT_H = SharedArrayManager.copy_array(var[:], np.float32)
        self.BT_H[self.BT_H == var._FillValue] = np.nan
        self.BT_H *= var.scale_factor
        self.BT_H += var.add_offset

        var = ncfile.variables["BT_V"]
        self.BT_V = SharedArrayManager.copy_array(var[:], np.float32)
        self.BT_V[self.BT_V == var._FillValue] = np.nan
        self.BT_V *= var.scale_factor
        self.BT_V += var.add_offset

        var = ncfile.variables["Pixel_Radiometric_Accuracy_H"]
        self.Pixel_Radiometric_Accuracy_H = SharedArrayManager.copy_array(var[:], np.float32)
        self.Pixel_Radiometric_Accuracy_H[self.Pixel_Radiometric_Accuracy_H == var._FillValue] = np.nan
        self.Pixel_Radiometric_Accuracy_H *= var.scale_factor
        self.Pixel_Radiometric_Accuracy_H += var.add_offset

        var = ncfile.variables["Pixel_Radiometric_Accuracy_V"]
        self.Pixel_Radiometric_Accuracy_V = SharedArrayManager.copy_array(var[:], np.float32)
        self.Pixel_Radiometric_Accuracy_V[self.Pixel_Radiometric_Accuracy_V == var._FillValue] = np.nan
        self.Pixel_Radiometric_Accuracy_V *= var.scale_factor
        self.Pixel_Radiometric_Accuracy_V += var.add_offset

        # self.Pixel_BT_Standard_Deviation_H = ncfile.variables['Pixel_BT_Standard_Deviation_H'][:].astype(np.float32)
        # self.Pixel_BT_Standard_Deviation_H[self.Pixel_BT_Standard_Deviation_H == ncfile.variables['Pixel_BT_Standard_Deviation_H']._FillValue ] = np.nan
        # self.Pixel_BT_Standard_Deviation_H *= ncfile.variables['Pixel_BT_Standard_Deviation_H'].scale_factor
        # self.Pixel_BT_Standard_Deviation_H += ncfile.variables['Pixel_BT_Standard_Deviation_H'].add_offset

        # self.Pixel_BT_Standard_Deviation_V = ncfile.variables['Pixel_BT_Standard_Deviation_V'][:].astype(np.float32)
        # self.Pixel_BT_Standard_Deviation_V[self.Pixel_BT_Standard_Deviation_V == ncfile.variables['Pixel_BT_Standard_Deviation_V']._FillValue ] = np.nan
        # self.Pixel_BT_Standard_Deviation_V *= ncfile.variables['Pixel_BT_Standard_Deviation_V'].scale_factor
        # self.Pixel_BT_Standard_Deviation_V += ncfile.variables['Pixel_BT_Standard_Deviation_V'].add_offset

        var = ncfile.variables["Incidence_Angle"]
        self.Incidence_Angle = SharedArrayManager.copy_array(var[:], np.float32)
        self.Incidence_Angle[self.Incidence_Angle == var._FillValue] = np.nan
        self.Incidence_Angle *= var.scale_factor
        self.Incidence_Angle += var.add_offset

        var = ncfile.variables["Days"]
        self.Days = SharedArrayManager.copy_array(var[:])
        self.Days[self.Days == var._FillValue] = 0

        var = ncfile.variables["UTC_Seconds"]
        self.UTC_Seconds = SharedArrayManager.copy_array(var[:])
        self.UTC_Seconds[self.UTC_Seconds == var._FillValue] = 0

        var = ncfile.variables["UTC_Microseconds"]
        self.UTC_Microseconds = SharedArrayManager.copy_array(var[:])
        self.UTC_Microseconds[self.UTC_Microseconds == var._FillValue] = 0

        del var
        ncfile.close()

    def getValidCellData(self, rowsj, colsj, upf):
        exitFlag = 0
        dayAndTime = []
        empty_angles = np.empty(0)
        TBH = np.empty(0)
        TBV = np.empty(0)

        if np.isnan(self.Grid_Point_Mask[rowsj, colsj]):
            exitFlag = 1  # no data
            return exitFlag, dayAndTime, empty_angles, TBH, TBV

        angles = self.Incidence_Angle[:, rowsj, colsj]
        accu_tbh = self.Pixel_Radiometric_Accuracy_H[:, rowsj, colsj]
        accu_tbv = self.Pixel_Radiometric_Accuracy_V[:, rowsj, colsj]
        # std_tbh = self.Pixel_BT_Standard_Deviation_H[:, rowsj, colsj]
        # std_tbv = self.Pixel_BT_Standard_Deviation_V[:, rowsj, colsj]
        TBH = self.BT_H[:, rowsj, colsj]
        TBV = self.BT_V[:, rowsj, colsj]

        # keep TB, only if ~NaN, angle within [20:55], and std < accuracy + 5
        with np.errstate(invalid='ignore'):
            indextb = np.nonzero(~np.isnan(TBH) & ~np.isnan(TBV) & (angles >= upf.th_min_angle[0]) & (angles <= upf.th_max_angle[0]))[0]

        # remove index 9 (39:41 deg), since it is already covered by 8 and 10 (35-40-45)
        indextb = indextb[indextb != 8]

        if indextb.size == 0:
            exitFlag = 2  # No data after first filtering
            return exitFlag, dayAndTime, empty_angles, TBH, TBV

        angles = angles[indextb]
        angle_separation = angles.ptp()

        if angle_separation < upf.th_min_angle_separation[0]:  # Eliminate group of TB when they are not separated at least 10º
            exitFlag = 3  # angle separation too narrow
            return exitFlag, dayAndTime, empty_angles, TBH, TBV

        # here the length of indextb > 0
        midIndex = indextb[int(np.ceil(len(indextb) / 2)) - 1]
        dayAndTime = [self.Days[midIndex, rowsj, colsj], self.UTC_Seconds[midIndex, rowsj, colsj], self.UTC_Microseconds[midIndex, rowsj, colsj]]
        if dayAndTime[0] < 0 or dayAndTime[1] < 0 or dayAndTime[2] < 0:
            exitFlag = 4 # date is not valid
            return exitFlag, dayAndTime, empty_angles, TBH, TBV

        TBH = TBH[indextb]
        TBV = TBV[indextb]
        # sigmah = std_tbh[indextb]
        # sigmav = std_tbv[indextb]

        return exitFlag, dayAndTime, angles, TBH, TBV
