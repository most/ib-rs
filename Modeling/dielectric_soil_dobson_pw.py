# -*- coding: utf-8 -*-

import cmath
import numpy as np
from .dielectric_purew0 import dielectric_purew0


def dielectric_soil_dobson_pw(data, par, upf):
    rhos = par.rhos
    rhob = par.rhob
    sm = par.sm
    s = par.s
    c = par.c

    f = upf.f_SMOS
    epsilon_winf = upf.epsilon_winf
    epsilon_0 = upf.epsilon_0
    alpha = upf.alpha

    cpa1 = upf.cpa1
    cpa2 = upf.cpa2
    cpa3 = upf.cpa3

    sgef1 = upf.sgef1
    sgef2 = upf.sgef2
    sgef3 = upf.sgef3
    sgef4 = upf.sgef4

    bere1 = upf.bere1
    bere2 = upf.bere2
    bere3 = upf.bere3

    beim1 = upf.beim1
    beim2 = upf.beim2
    beim3 = upf.beim3

    # in this module we are considering pure water in soil
    epsilon_pw0, pi2rtaupw = dielectric_purew0(data, par, upf) # Pure water

    epsilon_pa = (cpa1 + cpa2 * rhos) ** 2 + cpa3

    sigma_eff = sgef1 + sgef2 * rhob + sgef3 * s + sgef4 * c

    beta_re = bere1 + bere2 * s + bere3 * c
    beta_im = beim1 + beim2 * s + beim3 * c

    epsilon_over_pirtaupw_2 = (epsilon_pw0 - epsilon_winf) / (1 + (pi2rtaupw * f) ** 2)

    epsilon_fw_re = epsilon_winf + epsilon_over_pirtaupw_2

    sm_x_epsilon_fw_im = sm * pi2rtaupw * f * epsilon_over_pirtaupw_2 + sigma_eff / (2 * np.pi * f * epsilon_0) * (rhos - rhob) / rhos

    # It exists a lot of ambiguities regarding the way to interpret and compute
    # the dobson formula.
    # The expression using sm^beta, beta being a complex number is obviously
    # wrong if we compare results with the Dobson figures or tables.
    #
    # Here it has been verified that from initial Dobson model and experimental
    # measurements (IEEE on Geoscience and remote sensing 1985) that
    # 1st) regarding beta, the fits of beta are done separately for its real
    # part and imaginary part => thus the complex value sm^beta is not
    # sm powered to the complex beta but it's sm^beta' + i *
    # sm^beta".
    # 2nd) regarding epsilon_fw.^alpha to be used with the beta',beta" values
    # it is (epsilon_fw^alpha)'  (epsilon_fw'^alpha) generate always
    # smaller epsilon_soil" that disagrees with the Dobson measurements).
    #
    # The following expression is the one that agrees the best with the results
    # provided in the Dobson's paper.
    # epsilon_soil=((1+rhob./rhos.*(epsilon_pa.^alpha - 1) + sm.^(real(beta)-alpha).*real(sm_x_epsilon_fw.^alpha) - sm) ...
    #              + i*(sm.^(imag(beta)-alpha).*imag(sm_x_epsilon_fw.^alpha))).^(1/alpha);
    # Now (2007/03/03) it appears that finally the best choice is rather the 2nd form in term of the use of the exponent 1/alpha (rev 00.0.c)

    epsilon_soil_re = cmath.exp((1 / alpha) * cmath.log(1 + rhob / rhos * (epsilon_pa ** alpha - 1) + cmath.exp(beta_re * cmath.log(sm)) * epsilon_fw_re ** alpha - sm))
    epsilon_soil_im = cmath.exp((beta_im / alpha - 1) * cmath.log(sm)) * sm_x_epsilon_fw_im
    epsilon_soil = epsilon_soil_re.real - epsilon_soil_im.real * 1j

    return epsilon_soil
