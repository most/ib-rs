# -*- coding: utf-8 -*-

# function [rbh rbv]=fresnel(par,upf)
#
# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.theta: double x ND; incidence angles in degrees.
#       par.epsilon:complex x ND dielectric constant
#   upf: structure; contains the global User Parameter File external parameters
#       upf.mus: double x 1; relative magnetic permeability of the medium
#                            (soil or water here)
#
# Output:
#   rbh: double x ND; reflectivity at horizontal polarization
#   rbv: double x ND; reflectivity at vertical polarization
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute the Fresnesl smooth reflectivities (rbH and rbV) given the dielectric constant
# and the inicidence angles.
## Ref ATBD: TO ADD
#
# See also CAPITALIZE_THE_NAMES
#
#
# Comments:
# Pre-QR / QR specific not optimised code. It focuses on functions
# separation in order to ease the SMPPD verification process at the expense
# of efficiency and control.

import numpy as np


def fresnel(data, par, upf):
    mus = upf.mus
    theta = par.theta
    epsilon = data.epsilon
    cos_theta = data.cos_theta

    sq = np.sqrt(mus * epsilon - np.sin(np.deg2rad(theta)) ** 2)
    mc = mus * cos_theta
    ec = epsilon * cos_theta

    rbh = np.abs((mc - sq) / (mc + sq)) ** 2
    rbv = np.abs((ec - sq ) / (ec + sq)) ** 2

    return rbh, rbv
