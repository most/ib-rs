# -*- coding: utf-8 -*-

# function [tb_atm tau_atm]=atmosphere(par,upf)
#
# Input:
#   par: structure; cotains some inputs parameters to be used for this function
#       par.theta: double x ND array; incidence(s) angle(s)
#       par.t0_k: double x ND array; 2 meter temperature in Kelvin
#       par.p0: double x ND array; surface pressure in hPa
#       par.wvc: double x ND array;    Water Vapor Content in kg.m^-2
#   upf: structure; contains the global User Parameter File external parameters
#       xxx: double; all the Atmosphere regression parameters;
# Output:
#   tb_atm: double x ND array; effective brightness temperature of the atmosphere
#                               layer
#   tb_tua: double x ND array; effective optical thickness of the
#                               atmosphere layer
#
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute
#
# See also CAPITALIZE_THE_NAMES
#
# Authors: Philippe Richaume <philippe.richaume@cesbio.cnes.fr>
# Creation Date:  2006/07/10
# Revision: 00.0a PRE-QR/QR version Date: 2006/07/10
#
# Comments:
# Pre-QR / QR specific not optimised code. It focuses on functions
# separation in order to ease the SMPPD verification process at the expense
# of efficiency and control.
# Ref ATBD: TO ADD


def atmosphere(data, par, upf):
    theta = par.theta
    T0 = par.t0_k
    P0 = par.p0
    WVC = par.wvc
    cos_theta = data.cos_theta

    # TB/Tau oxgen
    k0_tau_O2 = upf.k0_tau_o2
    kT0_tau_O2 = upf.kt0_tau_o2
    kP0_tau_O2 = upf.kp0_tau_o2

    kT02_tau_O2 = upf.kt02_tau_o2
    kP02_tau_O2 = upf.kp02_tau_o2
    kT0P0_tau_O2 = upf.kt0p0_tau_o2

    tau_O2 = 1e-6 * (k0_tau_O2 + kT0_tau_O2 * T0 + kP0_tau_O2 * P0 + kT02_tau_O2 * T0 ** 2 + kP02_tau_O2 * P0 ** 2 + kT0P0_tau_O2 * T0 * P0) / cos_theta

    k0_DT_O2 = upf.k0_dt_o2
    kT0_DT_O2 = upf.kt0_dt_o2
    kP0_DT_O2 = upf.kp0_dt_o2
    kT02_DT_O2 = upf.kt02_dt_o2
    kP02_DT_O2 = upf.kp02_dt_o2
    kT0P0_DT_O2 = upf.kt0p0_dt_o2

    DTO2 = k0_DT_O2 + kT0_DT_O2 * T0 + kP0_DT_O2 * P0 + kT02_DT_O2 * T0 ** 2 + kP02_DT_O2 * P0 ** 2 + kT0P0_DT_O2 * T0 * P0

    TB_02 = (T0 - DTO2) * tau_O2

    # TB / Tau H2O
    k0_tau_H2O = upf.k0_tau_h2o
    k1_tau_H2O = upf.k1_tau_h2o
    k2_tau_H2O = upf.k2_tau_h2o

    tau_H2O = 1e-6 * (k0_tau_H2O + k1_tau_H2O * P0 + k2_tau_H2O * WVC) / cos_theta

    # idx=find(tau_H2O < 0); % tau_H2O=max(tau_H2O, 0);
    # np.where ?
    tau_H2O[tau_H2O < 0] = 0

    k0_DT_H2O = upf.k0_dt_h2o
    k1_DT_H2O = upf.k1_dt_h2o
    k2_DT_H2O = upf.k2_dt_h2o

    DTH2O = k0_DT_H2O + k1_DT_H2O * P0 + k2_DT_H2O * WVC

    TB_H2O = (T0 - DTH2O) * tau_H2O

    # final
    tb_atm = TB_H2O + TB_02
    tau_atm = tau_H2O + tau_O2

    return tb_atm, tau_atm
