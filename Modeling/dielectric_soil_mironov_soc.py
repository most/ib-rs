import math

def dielectric_soil_mironov_soc(data, par, upf):
    """
    Based on:
    V. L. Mironov, L. G. Kosolapova, S. V. Fomin and I. V. Savin,
    "Experimental Analysis and Empirical Model of the Complex Permittivity
    of Five Organic Soils at 1.4 GHz in the Temperature Range From
    -30 °C to 25 °C", IEEE TGRS, vol. 57, no. 6, pp. 3778-3787, June 2019.

    Input:
        par.sm: soil moisture volumic fraction (between 0 and 1)
        par.c: soil organic carbon (percentage between 0 and 100)
        par.tpw_c: water soil effective temperature in Celcius
    """
    sm = par.sm
    tC = data.tpw_c
    tC_2 = tC * tC
    SOC = par.c

    SOM = 1.724 * SOC
    rd = 0.14 + 1.5 * math.exp(-SOM / 13.7)
    sm_mg = sm / rd

    if tC < 0:
        sm_mg1 = 0.114 + 9.516e-4 * SOM + 1.23e-3 * tC
        sm_mg2 = 0.205 + 1.43e-3 * SOM + 0.187 * math.exp(tC / 6.6)
        nm = 0.507 + 1.24e-3 * tC
        nb = 2.941 + 0.0188 * tC
        nt = 8.371 + 0.304 * tC + 3.81e-3 * tC_2
        nui = 1.567 + 0.01263 * tC
        km = 7.65e-3 - 1.81e-4 * tC
        kb = 0.89 + 0.0185 * tC
        kt = 2.263 + 5.65e-3 * tC - 8.32e-4 * tC_2
        kui = 0.169 - 4.93e-3 * tC
    else:
        sm_mg1 = 0.118 + 8.695e-4 * SOM - 9.6e-4 * tC
        sm_mg2 = 0.382 + 9.208e-4 * SOM - 1.91e-3 * tC
        nm = 0.504 + 8.75e-7 * tC
        nb = 3.010 + 0.0328 * tC
        nt = 7.572 - 8.33e-4 * tC
        nui = 8.906 - 0.0207 * tC
        km = 0.0
        kb = 1.057 + 2.39e-3 * tC
        kt = 1.831 - 0.0252 * tC
        kui = 0.832 - 2.21e-2 * tC + 4.37e-4 * tC_2

    if sm_mg < sm_mg1:
        ns_ = nm + nb * sm_mg
        ks_ = km + kb * sm_mg
    elif sm_mg1 < sm_mg and sm_mg < sm_mg2:
        d_sm_mg = sm_mg - sm_mg1
        ns_ = nm + nb * sm_mg1 + nt * d_sm_mg
        ks_ = km + kb * sm_mg1 + kt * d_sm_mg
    elif sm_mg >= sm_mg2:
        d_sm_mg = sm_mg - sm_mg2
        d_sm_mg2 = sm_mg2 - sm_mg1
        ns_ = nm + kb * sm_mg1 + nt * d_sm_mg2 + nui * d_sm_mg
        ks_ = km + kb * sm_mg1 + kt * d_sm_mg2 + kui * d_sm_mg

    ns = ns_ * rd + 1
    ks = ks_ * rd

    epmx = ns * ns - ks * ks
    epmy = 2 * ns * ks

    epsilon_soil = epmx - epmy * 1j
    return epsilon_soil
