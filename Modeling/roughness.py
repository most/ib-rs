# -*- coding: utf-8 -*-

# function [rgh rgv] = roughness(par,upf)
#
# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.theta: double x ND; incidence angles in degrees.
#       par.qr: double x ND; QR polar coupling roughness param.
#       par.nrh: double x ND; NrH power law of cos(theta) parameter
#       par.nrv: double x ND; NrV power law of cos(theta) parameter
#       par.rbv: double x ND; RbV smooth Fresnel reflectivity for H polarization
#       par.rbh: double x ND; RbH smooth Fresnel reflectivity for V polarization
#   upf: structure; contains the global User Parameter File external parameters
#
# Output:
#   rgh: double x ND;  horizontal polarized rough reflectivity
#   rgv: double x ND;  vertical polarized rough reflectivity
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute  directional rough reflectivity from the inincidence angles, the smooth Fresnel reflectivity and the roughness parameters.
## Ref ATBD: TO ADD
#
# See also CAPITALIZE_THE_NAMES
# Comments:
# Pre-QR / QR specific not optimised code. It focuses on functions
# separation in order to ease the SMPPD verification process at the expense
# of efficiency and control.

import numpy as np
from .hr_sm import hr_sm


def roughness(data, par, upf):
    theta = par.theta
    cos_theta = data.cos_theta
    QR = par.qr
    RbV = data.rbv
    RbH = data.rbh
    NrH = par.nrh
    NrV = par.nrv

    m_hr_sm = -hr_sm(par, upf)
    rgh = ((1 - QR) * RbH + QR * RbV) * np.exp(m_hr_sm * cos_theta ** NrH)
    rgv = ((1 - QR) * RbV + QR * RbH) * np.exp(m_hr_sm * cos_theta ** NrV)

    return rgh, rgv
