# -*- coding: utf-8 -*-

# function [TB_H TB_V Tau_H Tau_V gamma_H gamma_V Tau_SH Tau_SV Tau_L TBSoilH TBSoilV TBVegH TBVegV]=tau_omega(par,upf)
#
# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.theta: double x ND; the incidence angles in degrees.
#       par.taus_nad:double x ND; The nadir optical thickness of the canopy layer, TauS_Nad
#       par.rgh: double x ND; the horizontal polarized rough reflectivity of the soil layer
#       rpar.rgv: double x ND; the vertical polarized rough reflectivity of the soil layer
#       par.tc_k: double x ND; the temperature of vegetation layer in Kelvin.
#       par.tg_k: double x ND; the temperature of ground layer in Kelvin
#       par.omega_h: double x ND; the horizontal polarized single scattering albedo of the vegetation layer
#       par.diff_omega: double x ND; the difference between horizontal and vertical polarized single scattering
#                                    albedo of the vegetation layer (changed see Rev 00.0d)
#       par.tt_h: double x ND; the horizontal polarized coupling factor linking Tau_H to TauS_Nad and theta.
#       par.rtt: double x ND; the ratio Tau_V/Tau_H linking TauV to TauS_Nad and theta. (changed see Rev 00.0d)
#       par.a_l: double x ND; first coefficient (a_L) linking the litter moisture, Mg_L, to the soil moisture SM
#       b_L=par.b_l: double x ND; second coefficient (b_L) linking the litter moisture, Mg_L, to the soil moisture SM
#       par.bsl: double x ND; Bs_L coefficient linking the litter water content (LWC) to the litter moisture Mg_L
#       par.cl: double x ND; cL coefficient linking the optical thickness of litter,Tau_L, to LWC
#       par.b_t: double x ND; Bt coefficient linking the effective composite soil-vegetation temperature to the soil temperature, the
#                             vegetation temperature and optical thickness
#       SM=par.sm: double x ND; soil moisture volumic fraction (belongs to [0 1])
#   upf: structure; contains the global User Parameter File external parameters
#
# Output:
#   TB_H: double x ND; above surface layer brightness temperature at horizontal polarization
#   TB_V: double x ND; above surface layer brightness temperature at vertical polarization
#   Tau_H: double x ND; above surface optical thickness at horizontal polarization
#   Tau_V: double x ND; above surface optical thickness at vertical polarization
#   Gamma_H: double x ND; above surface attenuation at horizontal polarization
#   Gamma_V: double x ND; above surface attenuation at vertical polarization
#   Tau_SH: double x ND; above surface optical thickness at horizontal polarization, vegetation layer
#   Tau_SV: double x ND; above surface optical thickness at vertical polarization, vegetation layer
#   Tau_L: double x ND; above surface optical thickness, litter layer
#   TBSoilH: double x ND; TB from soil layer in H pol (Phys temp * emissivity)
#   TBSoilV: double x ND; TB from soil layer in V pol (Phys temp * emissivity)
#   TBVegH: double x ND; TB from Vegetation layer in H pol
#   TBVegV: double x ND; TB from Vegetation layer in V pol
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute  the AGL TBH/V, incidence angle dependent, using Tau-Omega model
# on the vegetation properties, the rough soil reflectivities and the soil
# moisture.
## Ref ATBD: TO ADD
#
# See also CAPITALIZE_THE_NAMES
#
# Authors: Philippe Richaume <philippe.richaume@cesbio.cnes.fr>
# Creation Date:  2006/08/14
# Revision: 00.0a PRE-QR/QR version Date: 2006/08/14
# Comments: started.
# Revision: 00.0b PRE-QR/QR version Date: 2006/10/22
#   * changes: added tau litter, Tau_SH in ouput parametr
#   * changes: added tau litter, Tau_SV in ouput parametr
#   * changes: added tau litter, Tau_L in ouput parameter
# Revision: 00.0c PRE-QR/QR version Date: 2006/12/11
#   * fix oversigt; added the Mg_L min/max value control
#   * note: added to UPF upf.mgl_min & upf.mgl_max for Mg_L range in Tau_L computation
# Revision: 00.0d PRE-QR/QR version Date: 2006/12/20
#   * Change; tau_v is no longer a parameter, it has been replaced by Rtt i.e. Tau_V = Tau_H * Rtt to be strictly conform with to the ATBD
#   * Change; omega_v is no longer a parameter, it has been replaced by diff_omega i.e. Omega_V = Omega_H + diff_Omega to be strictly conform with the ATBD
# Revision: 00.0e AlgovalP Date: 2007/02/07
#   * Rename tau_nad to taus_nad to remind that tau_nad refers to standing vegetation Tau Nadir
# Revision: 00.0f AlgovalP Date: 2008/03/15
#   * split the final computation in soil TB and veg TB to reports thes intermediary results TBSoilH TBSoilV TBVegH TBVegV in output
# Revision: 00.0g AlgovalP Date: 2008/08/22
#   * moved SM=par.sm in a section controlled by cL existence and not nul (litter effect).
#     So that tau_omega can be called even on surface where litter parameter are not defined because with no meaning
#
# Comments:
# Pre-QR / QR specific not optimised code. It focuses on functions
# separation in order to ease the SMPPD verification process at the expense
# of efficiency and control.
# works for LV as well as for FO
# LV: Tc_K =Tg_K = Tgc_K, omega_H,  omega_V
# FO: Tc_K, Tg_K,  omega_H=omega_V=omega_F
#
# AlgoValP: 2007
# new set of modifications introduced to cover the issues found.

import numpy as np


def tau_omega(data, par, upf):
    TauS_nad = par.taus_nad
    theta = par.theta
    rg_H = data.rgh
    rg_V = data.rgv
    Tc_K = par.tc_k
    Tg_K = data.tg_k
    omega_H = par.omega_h
    omega_V = par.omega_v
    tt_H = par.tt_h
    tt_V = par.tt_v
    Mg_L_max = upf.mgl_max[0]
    Mg_L_min = upf.mgl_min[0]

    c = data.cos_theta
    c2 = c ** 2
    s2 = 1 - c2
    Tau_SH = TauS_nad * (c2 + tt_H * s2)
    Tau_SV = TauS_nad * (c2 + tt_V * s2)

    # note: to control the use of litter for FFO a_L and b_L should be set
    #accordingly in LAND_COVER_CLASSES for LC forest codes.
    ## Rev 00g
    if par.cl is not None and par.cl != 0:
        cL = par.cl
        Bs_L = par.bsl
        SM = par.sm
        a_L = par.a_l
        b_L = par.b_l
        Mg_L = max(min(a_L * SM + b_L, Mg_L_max), Mg_L_min) # min(),max() added here see Rev 00.0c
        LWC = Mg_L / (1 - Mg_L) * Bs_L
        Tau_L = cL * LWC
        Tau_H = Tau_SH + Tau_L
        Tau_V = Tau_SV + Tau_L
    else:
        Tau_L = 0
        Tau_H = Tau_SH
        Tau_V = Tau_SV

    gamma_H = np.exp(-Tau_H / c)
    gamma_V = np.exp(-Tau_V / c)

    # Reminder: Here, note that the following depends if we are computing the
    # layer for low vegetation or for forest.
    # Especially the par.tc_k and par.tg_k parameters have to be set according
    # to :
    # LV: Tc_K=Tg_K = Tgc_K
    # FO: Tc_K, Tg_K

    ##Rev 0.0f

    TBVegH = (1 - omega_H) * (1 - gamma_H) * (1 + gamma_H * rg_H) * Tc_K
    TBVegV = (1 - omega_V) * (1 - gamma_V) * (1 + gamma_V * rg_V) * Tc_K

    TBSoilH = (1 - rg_H) * Tg_K
    TBSoilV = (1 - rg_V) * Tg_K

    TB_H = TBVegH + TBSoilH * gamma_H
    TB_V = TBVegV + TBSoilV * gamma_V

    return TB_H, TB_V, Tau_H, Tau_V, gamma_H, gamma_V, Tau_SH, Tau_SV, Tau_L, TBSoilH, TBSoilV, TBVegH, TBVegV
