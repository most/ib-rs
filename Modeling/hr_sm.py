# -*- coding: utf-8 -*-

# function hr=hr_sm(par,upf)
#
# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.sm: double x ND; fraction of volumic soil moisture (belongs to
#               [0 1])
#       par.hr: double x ND; HR param for the piecewise function HR(SM).
#       par.hr_min: double x ND; HR_min param for the piecewise function
#                                HR(SM).
#       par.xmvt: double x ND; XMVT param for the piecewise function
#                              HR(SM).
#       par.fc: double x ND; FC param (field capacity) for the piecewise function
#                            HR(SM).
#
#   upf: structure; contains the global User Parameter File external parameters
#
# Output:
#
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute  HR as a function of SM.
## Ref ATBD: TO ADD
#
# See also CAPITALIZE_THE_NAMES
#
# Authors: Philippe Richaume <philippe.richaume@cesbio.cnes.fr>
# Creation Date:  2006/07/25
# Revision: 00.0a PRE-QR/QR version Date: 2006/07/25
# Revision: 00.0b PRE-QR/QR version Date: 2006/10/17
#   * added scalar handling of HR_max
# Revision: 00.0c Date: 2008/08/11
#   * added general size handling as call may be far much complex and mixed
#     scalar / vector type for various parameters
# Revision: 00.0d Date: 2008/08/22
#   * if no sm field => HR(SM) no relevant => return par.hr_max.
# Revision: 00.0d-alpha Date: 2009/09/30
#   * bug fixed: already fixed in the BB_R6 and given there as an interim fix. Bad resizing
#                in the dimension harmonizing section.
# Revision: 00.0d-alpha Date: 2011/01/30
#   * bug fixed: 00.0c not totally fixed problems for some parameter, now
#     done.
#
# Comments:
# Pre-QR / QR specific not optimised code. It focuses on functions
# separation in order to ease the SMPPD verification process at the expense
# of efficiency and control.
#
# For forward modelling the piecewise form is usable.
# For retrieval the function should be converted in a class C1 function
# i.e a function having its 1st order derivative continupus in sm: TO DO
#
# Otherwise it will propagate numerical instabilities when computing in LVM
# algorithm D(HR(SM))/D(SM) when SM crosses the two points, XMVT and FC.
#
# We can note that since HR(SM) is at least C0, these instabilities are not the
# worst case. The finite difference will introduce at worst a delta_HR/delta_SM
# when crossing XMVT or FC that results in a smaller slope (so no
# divergence)

import numpy as np


def hr_sm(par, upf):
    # version scalaire
    VER = '00d-alpha'
    if par.sm is None:
        hr = par.hr
    else:
        SM = par.sm
        HR_max = par.hr
        HR_min = par.hr_min
        XMVT = par.xmvt
        FC = par.fc

        if SM > FC:
            hr = HR_min
        else:
            a = (HR_max - HR_min) / (XMVT - FC)
            hr = (SM - XMVT) * a + HR_max

    return hr
