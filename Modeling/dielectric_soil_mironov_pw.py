# -*- coding: utf-8 -*-

# Input:
#   par: structure contains some inputs parameters to be used for this function
#       par.sm: double x ND soil moisture volumic fraction (between [0 1])
#       par.c double x ND clay volumic fraction (between [0 1])
#       par.tpw_c double x ND water soil effective temperature in Celcius !!!
#
#   upf: structure contains the global User Parameter File external parameters
#       xxx: double x 1 many external parameters see ATBD/TGDR.
# Output:
#   epsilon_soil: complex x ND soil dielectic constant
#   epsilon_pw0:
#   pi2rtaupw:
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute  the soil dielectic constant using a pure water model for the
# soil moisture.

import math


def dielectric_soil_mironov_pw(data, par, upf):
    sym = upf.sym[0]
    sm1 = upf.sm1[0]
    tpoly = upf.tpoly[0]
    sm = par.sm
    tC = data.tpw_c
    c = par.c
    zero_c_in_k = upf.zero_c_in_k
    frequ = upf.f_SMOS
    permit0 = upf.permit0   # 8.854e-12,'mironov_model'
    epwi0 = upf.epwi0       # 4.9,'mironov_model'
    nd0 = upf.nd0           # 1.62847'mironov_model'
    nd1 = upf.nd1           # -0.70803'mironov_model'
    nd2 = upf.nd2           # 0.4659'mironov_model'
    kd0 = upf.kd0           # 0.03945'mironov_model'
    kd1 = upf.kd1           # -0.03721'mironov_model'
    xmvt0 = upf.xmvt0       # 0.00625'mironov_model'
    xmvt1 = upf.xmvt1       # 0.33918'mironov_model'
    tf0 = upf.tf0           # 20'mironov_model'
    e0pb0 = upf.e0pb0       # 79.82918'mironov_model'
    e0pb1 = upf.e0pb1       # -85.36581'mironov_model'
    e0pb2 = upf.e0pb2       # 32.70444'mironov_model'
    bvb0 = upf.bvb0         # -5.96311e-19'mironov_model'
    bvb1 = upf.bvb1         # -1.25999e-3'mironov_model'
    bvb2 = upf.bvb2         # 1.83991e-3'mironov_model'
    bvb3 = upf.bvb3         # -9.77347e-4'mironov_model'
    bvb4 = upf.bvb4         # -1.39013e-7'mironov_model'
    bsgb0 = upf.bsgb0       # 0.0028'mironov_model'
    bsgb1 = upf.bsgb1       # 2.37388e-2'mironov_model'
    bsgb2 = upf.bsgb2       # -2.93876e-2'mironov_model'
    bsgb3 = upf.bsgb3       # 3.28954e-2'mironov_model'
    bsgb4 = upf.bsgb4       # -2.00582e-2'mironov_model'
    dhbr0 = upf.dhbr0       # 1466.80741'mironov_model'
    dhbr1 = upf.dhbr1       # 26.97032e2'mironov_model'
    dhbr2 = upf.dhbr2       # -0.09803e4'mironov_model'
    dsrb0 = upf.dsrb0       # 0.88775'mironov_model'
    dsrb1 = upf.dsrb1       # 0.09697e2'mironov_model'
    dsrb2 = upf.dsrb2       # -4.2622'mironov_model'
    taub0 = upf.taub0       # 48e-12'mironov_model'
    sbt0 = upf.sbt0         # 0.29721'mironov_model'
    sbt1 = upf.sbt1         # 0.49'mironov_model'
    e0pu = upf.e0pu         # 100'mironov_model'
    bvu0 = upf.bvu0         # 1.10511e-4'mironov_model'
    bvu1 = upf.bvu1         # -5.16834e-6'mironov_model'
    bsgu0 = upf.bsgu0       # 0.00277'mironov_model'
    bsgu1 = upf.bsgu1       # 3.58315e-2'mironov_model'
    dhur0 = upf.dhur0       # 2230.20237'mironov_model'
    dhur1 = upf.dhur1       # -0.39234e2'mironov_model'
    dsur0 = upf.dsur0       # 3.6439'mironov_model'
    dsur1 = upf.dsur1       # -0.00134e2'mironov_model'
    tauu0 = upf.tauu0       # 48e-12'mironov_model'
    sut0 = upf.sut0         # 0.12799'mironov_model'
    sut1 = upf.sut1         # 1.65164'mironov_model'

    c2 = c * c
    c3 = c2 * c
    c4 = c2 * c2

    # function epsol=mironov(fr,tsol,xmv,clay)
    # mironov (1.4e9, 300, 0.3, 0.14)

    # compute the soil complex dielectric constant (epsol)
    #     Mironov et al. 2007 model , submitted.
    #     code by JPW, Aug. 2007

    # fr = frequency in Hz !
    # tsol soil temperature in K
    # xmv = soil volumetric moisture content (m3./m3)
    # clay = gravimetric fraction of clay

    # Oct.09 code updated by H.Lawrence and C.Duffour to include Tsol:
    #		- spectroscopic parameters ep0u, ep0b,taub,tauu were changed to the formulae in Mironov et al PIERS proc. 2009 to include Tsol
    #		- new spectroscopic parameters were used: these will be published by Mironov et al in the TGRS conference paper
    # PAR PERMIT0=8.854e-12
    # permit=8.854e-12;

    # PAR EPWI0=4.9
    # epwi=4.9;

    tK = tC + zero_c_in_k
    delta_tC_tf0 = tC - tf0

    # MIRONOV formulation

    # -----------------------------------------------------------
    # Initializing the GRMDM spectroscopic parameters with clay (fraction)

    # RI & NAC of dry soils
    # PAR ND0=1.62847 ND1=-0.70803 ND2=0.4659
    nd = nd0 + nd1 * c + nd2 * c2

    # PAR KD0=0.03945 KD1=-0.03721
    kd = kd0 + kd1 * c
    # kd = 0.03952 - 0.04038 .* clay; older version in submitted paper

    # maximum bound water fraction
    # PAR XMVT0=0.00625  XMVT1=0.33918
    xmvt = xmvt0 + xmvt1 * c

    # bound water parameters
    # PAR TF0=20;
    # t=20; starting temperature for parameters' fit

    # ep0b computation
    # PAR E0PB0=79.82918 E0PB1=-85.36581 E0PB2=32.70444
    e0pb = e0pb0 + e0pb1 * c + e0pb2 * c2

    # PAR BVB0=-5.96311e-19 BVB1=-1.25999e-3 BVB2=1.83991e-3 BVB3=-9.77347e-4 BVB4=-1.39013e-7
    Bvb = bvb0 + bvb1 * c + bvb2 * c2 + bvb3 * c3 + bvb4 * c4

    # PAR BSGB0=0.0028 BSGB1=2.37388e-2 BSGB2=-2.93876e-2 BSGB3=3.28954e-2 BSGB4=-2.00582e-2
    Bsgb = bsgb0 + bsgb1 * c + bsgb2 * c2 + bsgb3 * c3 + bsgb4 * c4

    Fpb = math.log((e0pb - 1) / (e0pb + 2))
    exp_Fpb = math.exp(Fpb - Bvb * delta_tC_tf0)
    ep0b = (1 + 2 * exp_Fpb) / (1 - exp_Fpb)

    # # taub computation
    # PAR DHBR0=1466.80741 DHBR1=26.97032e2 DHBR2=-0.09803e4
    dHbR = dhbr0 + dhbr1 * c + dhbr2 * c2

    # PAR DSRB0=0.88775 DSRB1=0.09697e2 DSRB2=4.2622
    dSbR = dsrb0 + dsrb1 * c + dsrb2 * c2

    # PAR TAUB0=48e-12
    taub = taub0 * math.exp(dHbR / tK - dSbR) / tK

    # # sigmab computation
    # PAR SBT0=0.29721 SBT1=0.49
    sigmabt = sbt0 + sbt1 * c
    sigmab = sigmabt + Bsgb * delta_tC_tf0

    # unbound (free) water parameters
    # # ep0u computation
    # PAR E0PU=100
    # e0pu = 100;

    # PAR BVU0=1.10511e-4 BVU1=-5.16834e-6
    Bvu = bvu0 + bvu1 * c

    # PAR BSGU0=0.00277 BSGU1=3.58315e-2
    Bsgu = bsgu0 + bsgu1 * c
    Fpu = math.log((e0pu -1) / (e0pu + 2))
    exp_Fpu = math.exp(Fpu - Bvu * delta_tC_tf0)
    ep0u = (1 + 2 * exp_Fpu) / (1 - exp_Fpu)

    # # tauu computation
    # PAR DHUR0=2230.20237 DHUR1=-0.39234e2
    dHuR = dhur0 + dhur1 * c
    # PAR DSUR0=3.6439 DSUR1=-0.00134e2
    dSuR = dsur0 + dsur1 * c
    # PAR TAUU0=48e-12
    tauu = tauu0 * math.exp(dHuR / tK - dSuR) / tK

    # # sigmau computation
    # PAR SUT0=0.12799 SUT1=1.65164
    sigmaut = sut0 + sut1 * c
    sigmau = sigmaut + Bsgu * delta_tC_tf0

    # ######       cx=(epw0-epwi)./(1.+i.*topi.*frequ);
    # ######       epfw=epwi+cx-i.*seff.*(ros-rob)./(2.*pi.*frequ.*permit.*ros.*xmv);

    # -----------------------------------------------------------
    # computation of epsilon water (bound & unbound)
    twopiF = 2 * math.pi * frequ
    twopiFtaub = twopiF * taub
    twopiFP0 = twopiF * permit0

    cxb = (ep0b - epwi0) / (1 + twopiFtaub * twopiFtaub)
    epwbx = epwi0 + cxb
    epwby = cxb * twopiFtaub  + sigmab / twopiFP0

    twopiFtauu = twopiF * tauu
    cxu = (ep0u - epwi0) / (1 + twopiFtauu * twopiFtauu)
    epwux = epwi0 + cxu
    epwuy = cxu * twopiFtauu + sigmau / twopiFP0

    # -----------------------------------------------------------
    # computation of refractive index of water (bound & unbound):
    sqrt_2 = 1.4142135623730951

    epwbnorm = math.sqrt(epwbx * epwbx + epwby * epwby)
    nb = math.sqrt(epwbnorm + epwbx) / sqrt_2
    kb = math.sqrt(epwbnorm - epwbx) / sqrt_2

    epwunorm = math.sqrt(epwux * epwux + epwuy * epwuy)
    nu = math.sqrt(epwunorm + epwux) / sqrt_2
    ku = math.sqrt(epwunorm - epwux) / sqrt_2

    # -----------------------------------------------------------
    # computation of soil refractive index (nm & km):
    # xmv can be a vector

    # lig = (xmv >= xmvt) ? 1.0 : 0.0;
    # sym=0;
    if sym == 0:
        # # 'std'
        xmvt2 = min(sm, xmvt)
        nm = nd + (nb - 1) * xmvt2 + (sm >= xmvt) * (nu - 1) * (sm - xmvt)
        km = kd + kb * xmvt2 + (sm >= xmvt) * ku * (sm - xmvt)

        # -----------------------------------------------------------
        # computation of soil dielectric constant:

        epmx = nm * nm - km * km    # real part
        epmy = nm * km * 2      # imaginary part
        epsilon_soil = epmx - epmy * 1j

    return epsilon_soil

 # else:
 #    	## 'symetrized'
 #    # 	sm = abs(sm)
 #    # 	sz = max([sm.shape, c.shape, tC.shape])
 #    # epsilon_soil=NaN(sz);
 #    # idx1 = find(sm <= sm1);
 #    # #     tpoly=1;
 #    # if ~isempty(idx1);
 #    #     ex0=nd.^2 - kd.^2;
 #    #     ey0=2*kd.*nd;

 #    #     ex1=(nd + sm1*(nb - 1)).^2 - (kd + kb*sm1).^2;
 #    #     ey1=(kd + kb*sm1).*(2*nd + 2*sm1.*(nb - 1));

 #    #     dex1=2*(nd + sm1*(nb - 1)).*(nb - 1) - 2*kb.*(kd + kb*sm1);
 #    #     dey1=2*(kd + kb*sm1).*(nb - 1) + 2*kb.*(nd + sm1*(nb - 1));

 #    #     if length(ex1(:))==1
 #    #         idx10=1;
 #    #     else
 #    #         idx10=idx1;
 #    #     end
 #    # 		if tpoly == 0:
 #    # 	# 'V0: aX2 + bX + c'
 #    # 	dx01 = ex0 - ex1
 #    # 	dy01 = ey0 - ey1
 #    # 	ax = -dx01 / np.pow(sm1, 2)
 #    # 	ay = -dy01 / np.pow(sm1, 2)
 #    # 	bx = ax
 #    # 	bx = 0
 #    # 	by = bx
 #    # 	cx = ex0
 #    # 	cy = ey0
 #    # 	epmx = ax[idx10].*sm(idx1).^2+bx(idx10).*sm(idx1)+cx(idx10);
 #    #             epmy=ay(idx10).*sm(idx1).^2+by(idx10).*sm(idx1)+cy(idx10);
 #    #         case 1
 #    #             %% 'V1: aX2+ bX + c'
 #    #             ax=0.5*dex1/sm1;
 #    #             ay=0.5*dey1/sm1;

 #    #             bx=ax;bx(:)=0;
 #    #             by=bx;

 #    #             cx=ex1-0.5*dex1*sm1;
 #    #             cy=ey1-0.5*dey1*sm1;

 #    #             epmx=ax(idx10).*sm(idx1).^2+bx(idx10).*sm(idx1)+cx(idx10);
 #    #             epmy=ay(idx10).*sm(idx1).^2+by(idx10).*sm(idx1)+cy(idx10);

 #    #         case 2
 #    #             %% 'aX3+bX2+cX+d'
 #    #             dx01=ex0-ex1;
 #    #             dy01=ey0-ey1;

 #    #             ax=(2*dx01+dex1*sm1)/sm1^3;
 #    #             ay=(2*dy01+dey1*sm1)/sm1^3;

 #    #             bx=-(3*dx01+dex1*sm1)/sm1^2;
 #    #             by=-(3*dy01+dey1*sm1)/sm1^2;

 #    #             cx=ax;
 #    #             cx(:)=0;
 #    #             cy=cx;

 #    #             dx=ex0;
 #    #             dy=ey0;
 #    #             epmx=ax(idx10).*sm(idx1).^3+bx(idx10).*sm(idx1).^2+cx(idx10).*sm(idx1)+dx(idx10);
 #    #             epmy=ay(idx10).*sm(idx1).^3+by(idx10).*sm(idx1).^2+cy(idx10).*sm(idx1)+dy(idx10);

 #    #         case 3
 #    #             %% 'aX4+bX2+c'
 #    #             dx01=ex0-ex1;
 #    #             dy01=ey0-ey1;

 #    #             ax=(dx01+0.5*dex1*sm1)/sm1^4;
 #    #             ay=(dy01+0.5*dey1*sm1)/sm1^4;

 #    #             bx=-(2*dx01+0.5*dex1*sm1)/sm1^2;
 #    #             by=-(2*dy01+0.5*dey1*sm1)/sm1^2;

 #    #             cx=ex0;
 #    #             cy=ey0;

 #    #             epmx=ax(idx10).*sm(idx1).^4+bx(idx10).*sm(idx1).^2+cx(idx10);
 #    #             epmy=ay(idx10).*sm(idx1).^4+by(idx10).*sm(idx1).^2+cy(idx10);
 #    #         otherwise
 #    #             error('tpoly (%d) is unknow',tpoly);
 #    #     end
 #    #     epsilon_soil(idx1)=epmx - 1i.*epmy;
 #    # end
 #  #   idx2 = where(sm > sm1)
 #  #   if len(idx2) > 0:
 #  #   	if len(nd) == 1:
 #  #   		idx20 = 1
 #  #   	else:
 #  #   		idx20 = idx2

 #  #   	if len(xmvt) == 1:
 #  #   		idx21 = 1
 #  #   	else:
 #  #   		idx21 = idx2

 #  #   	xmvt2 = min(sm[idx2], xmvt[idx21])
 #  #   	nm = nd[idx20] + (nb[idx20] - 1.) * xmvt2 + (sm[idx2] >= xmvt[idx21]) * (nu[idx20] - 1.) * (sm[idx2] - xmvt[idx21])
	# 	# km = kd[idx20] + kb[idx20] * xmvt2 + (sm[idx2] >= xmvt[idx21]) * ku[idx20] * (sm[idx2] - xmvt[idx21])

 #  #   	# -----------------------------------------------------------
 #  #   	# computation of soil dielectric constant:

 #  #   	epmx = nm * nm - km * km    # real part
 #  #   	epmy = nm * km * 2.         # imaginary part
 #  #   	epsilon_soil[idx2] = epmx - epmy * 1j
