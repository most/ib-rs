# -*- coding: utf-8 -*-

# function [toa_tbh toa_tbv]=toa_tb(par,upf)
#
# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.theta: double x ND; incidence angle in degrees.
#       par.tau_atm: double x ND; effective optical thicknes of the atmosphere
#       par.tb_atm: double x ND; effective brighntess temperature of the atmosphere
#       par.tb_sky_h: double x ND; horizontal polarized brightness temperature of the deep sky.
#       par.tb_sky_v: double x ND; vertical polarized brightness temperature of the deep sky.
#       par.rgh: double x ND; the horizontal polarized rough reflectivity of the surface layer
#       rpar.rgv: double x ND; the vertical polarized rough reflectivity of the surface layer
#       par.gammah: double x ND; exp(-Tau_H/cos(theta)), Tau_H being the horizontal polarized optical thickness of the surface layer
#       par.gammav: double x ND; exp(-Tau_H/cos(theta)), Tau_H being the vertical polarized optical thickness of the surface layer
#       par.asl_tbh: double x ND; the horizontal polarized brightness temperature above the surface layer
#       par.asl_tbv: double x ND; the vertical polarized brightness temperature above the surface layer
#   upf: structure; contains the global User Parameter File external parameters
#
# Output:
#   toa_tbh: double x ND; top of atmosphere brihgtness temperature at horizontal polarization
#   toa_tbv: double x ND; top of atmosphere brihgtness temperature at vertical polarization
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute the TOA TBH/V, incidence angle dependent, from the AGL TBH/V, the
# surface rough relectivities and the atmosheric properties (TB_ATM and
# Tau_ATM) and the deep sky TBH/V
#
## Ref ATBD: TO ADD
#
# See also CAPITALIZE_THE_NAMES
#
# Authors: Philippe Richaume <philippe.richaume@cesbio.cnes.fr>
# Creation Date:  2006/08/14
# Revision: 00.0a PRE-QR/QR version Date: 2006/08/14
# Revision: 00.0b PRE-QR/QR version Date: 2006/10/13
#   fix: gamma_h to gamma_h.^2 to obtain the correct double attenuation
#        downward and upward => gamma = exp(-Tau/cos(theta), so we change
#        to gamma^2 to obtaine the correct exp(-2*Tau/cos(theata))
#   fix: gamma_v to gamma_v.^2, same as above for v polarization
#
# Comments:
# Pre-QR / QR specific not optimised code. It focuses on functions
# separation in order to ease the SMPPD verification process at the expense
# of efficiency and control.

import numpy as np


def toa_tb(data, par, upf):
    tau_atm = data.tau_atm
    tb_atm = data.tb_atm
    tb_sky_h = par.tb_sky_h
    tb_sky_v = par.tb_sky_v
    rgh = data.rgh
    rgv = data.rgv
    tt_H = par.tt_h
    tt_V = par.tt_v

    if data.gamma_h is None or data.gamma_v is None:
        c = data.cos_theta
        vc = 1 / c - c

        gamma_h = np.exp(- par.taus_nad * (c + tt_H * vc))
        gamma_v = np.exp(- par.taus_nad * (c + tt_V * vc))
        data.gamma_h = gamma_h
        data.gamma_v = gamma_v
    else:
        gamma_h = data.gamma_h
        gamma_v = data.gamma_v

    asl_tb_h = data.asl_tbh
    asl_tb_v = data.asl_tbv
    exp_tau_atm = np.exp(-tau_atm)
    toa_tbh = tb_atm + exp_tau_atm * (tb_atm + tb_sky_h * exp_tau_atm) * rgh * gamma_h ** 2 + exp_tau_atm * asl_tb_h
    toa_tbv = tb_atm + exp_tau_atm * (tb_atm + tb_sky_v * exp_tau_atm) * rgv * gamma_v ** 2 + exp_tau_atm * asl_tb_v

    return toa_tbh, toa_tbv
