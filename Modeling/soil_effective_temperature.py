# -*- coding: utf-8 -*-

# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.sm: double x ND; volumetric soil fraction (belongs to [0 1])
#       par.w0: double x ND; texture parameter w0
#       par.bw0: double x ND; texture paramter bw0
#       par.t_soil_deep: double x ND; deep soil temperature in K
#       par.t_soil_surf: double x ND; surface soil temperature in K
#   upf: structure; contains the global User Parameter File external parameters
#
# Output:
#   The ground effective temperature.
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute the effective soil temperature from the deep soil temperature,
# the surface temperature using SM and texture parameter for coupling the
# two temperatures.


def soil_effective_temperature(par, upf):
    VER = '00b'
    SM = par.sm
    w0 = par.w0
    bw0 = par.bw0
    t_soil_deep = par.t_soil_deep
    t_soil_surf = par.t_soil_surf
    ct = min(((max(SM, 0) / w0) ** bw0), 1)
    tg = t_soil_deep + ct * (t_soil_surf - t_soil_deep)
    return tg
