# -*- coding: utf-8 -*-

# The following long list are the necessary dataameters to provide to this
# function and children functions call to be used in a simple forward call
# as well in an embedded retrieval module.
#
# Some usual dataameters of several sub-functions are not mentionned
# because they are set from other ones. It's the case for example of the usual
# data.tg_k that is computed from data.t_soil_surf & data.t_soil_deep that
# appear instead in the interface.
#
# Some of them are duplicated (duplications are signaled with a beginning
# '#') just to remind where they are used in various subfunctions calls.
# Of course only one instance is required.
#
# Input:
#   data: structure; contains the input dataameters to be used for this function
## General dataameters
#       data.model: string: the name of the dielectric model to use in
#                           'FNO','FFO','FWL','FWP','FWS','FEB','FTI','FEU',
#                           'FRZ','FSN','FMD','FNOP','FNOS','FFOP','FFOS',
#                           'FWLP','FWLS'
#       data.model_subtype: the type of dielectric constant model: Dobson='DOBS' or Mironov='MIRO'
#       data.theta: double x ND; incidences angles in degrees.
#
## Effective Soil Temperature
#       data.sm: double x ND; volumetric soil fraction (belongs to [0 1])
#       data.w0: double x ND; texture dataameter w0
#       data.bw0: double x ND; texture dataamter bw0
#       data.t_soil_surf: double x ND; surface soil temperature in K
#       data.t_soil_deep: double x ND; deep soil temperature in K
#
## Effective Composite Soil + Low Vegetation Temperature
#       data.taus_nad: double x ND; Nadir Optical thickness
#       data.b_t: double x ND; Bt coupling dataameter.
#       data.tc_k: double x ND; canopy temperature in K
#
## Dielectric Models dataameters (one or some of the set depending on the selected surface model)
##      [1] Nominal Soil Model With Pure Water Model + [3] Pure Water Model
#           #data.sm: double x ND; soil moisture volumic fraction (between [0 1])
#           data.rhos: double x ND; soil dataticle size
#           data.rhob: double x ND; soil bulk density
#           data.s; double x ND; sand volumic fraction (between [0 1])
#           data.c; double x ND; clay volumic fraction (between [0 1])
#
##      [2] Nominal Soil Model With Saline Water Model: Same as [1] + [4] Saline Water Model + the following
#           data.ssal; double x ND; soil salinity in
#
##      [3] Pure Water Model
#           data.tpw_c; double x ND; the pure water temperature in C
#
##      [4] Saline Water Model
#           data.tsw_c; double x ND: the saline water temperature in C
#           data.sal; double x ND; the water salinity in
#
##      [5] Cardioid Model
#           data.a_card: double x ND; cardioid A_Card dataameter
#           data.u_card; double x ND; cardioid U_card dataameter
#
##      [6] Other Dielectric Are Controlled Through UPF values (FRZ,FEU,FEB,FTI,FNS)
#
## Fresnel Model
#       #data.theta: double x ND; incidence angles in degrees.
#
## Roughness Model
#       #data.theta: double x ND; incidence angles in degrees.
#       data.qr: double x ND; QR polar coupling roughness dataam.
#       data.nrh: double x ND; NrH power law of cos(theta) dataameter
#       data.nrv: double x ND; NrV power law of cos(theta) dataameter
#
#
## Tau-Omega Model dataameters (For FNO & FFO)
#       #data.theta: double x ND; the incidence angles in degrees.
#       #data.taus_nad:double x ND; The nadir optical thickness of the canopy layer, TauS_Nad
#       #data.sm: double x ND; soil moisture volumic fraction (belongs to [0 1])
#       data.tc_k: double x ND; the temperature of vegetation layer in Kelvin.
#       data.omega_h: double x ND; the horizontal polarized single scattering albedo of the vegetation layer
#       data.omega_v: double x ND; the vertical polarized single scattering albedo of the vegetation layer
#       data.tt_h: double x ND; the horizontal polarized coupling factor linking Tau_H to TauS_Nad and theta.
#       data.tt_v: double x ND; the vertical polarized coupling factor linking Tau_H to TauS_Nad and theta.
#       data.a_l: double x ND; first coefficient (a_L) linking the litter moisture, Mg_L, to the soil moisture SM
#       data.b_l: double x ND; second coefficient (b_L) linking the litter moisture, Mg_L, to the soil moisture SM
#       data.bsl: double x ND; Bs_L coefficient linking the litter water content (LWC) to the litter moisture Mg_L
#       data.cl: double x ND; cL coefficient linking the optical thickness of litter,Tau_L, to LWC
#       data.b_t: double x ND; Bt coefficient linking the effective composite soil-vegetation temperature to the soil temperature, the
#                             vegetation temperature and optical thickness
#
## Amosphere Model dataameters
#       data.t0_k: double x 1; 2 meters temperature in Kelvin for atmosphere model
#       data.p0: double x 1; Surface pressure in hPa for atmosphere model
#       data.wvc: double x 1; Water Vapor Content in kg.m^-2 for atmosphere model
#
## Deep Sky dataameters
#       data.tbh_sky: double x ND; ND array of deep sky horizontally polarized brightness temperatures
#       data.tbv_sky: double x ND; ND array of deep sky vertically polarized brightness temperatures
#
#   upf: structure; contains the global User dataameter File external dataameters
#
##
# Output:
#   TOA_TBH: double x ND; ND array of TOp Atmosphere brightness temperature, H pol
#   TOA_TBV: double x ND; ND array of TOp Atmosphere brightness temperature, V pol
#   data: structure; the provided input data + added intermediary results,
#                   the following fields may be present or not depending of
#                   the selected surface model:
#       data.tge_k: double x ND; the effective soil temperature; if present, it means
#                               that data.tg_k has been changed to the effective composite
#                               soil+vegetation temperature.
#       data.tg_k: double x ND; the effective soil temperature or the effective composite
#                              the effective composite (see just above)
#       data.tpw_c: double x ND; the pure water temperature in degree celcius.
#       data.tsw_c: double x ND; the saline water temperature in degree celcius.
#       data.epsilon: complex x ND; the modelled surface dielectric constant.
#       data.rbh: double x ND; the smooth Fresnel reflectivity, H pol
#       data.rbv: double x ND; the smooth Fresnel reflectivity, V pol
#       data.rgh: double x ND; the rough reflectivity, H pol
#       data.rgv: double x ND; the rough reflectivity, V pol
#       data.asl_tbh: double x ND; the ASL brighntess temperature, H pol
#       data.asl_tbv: double x ND; the ASL brighntess temperature, V pol
#       data.tau_h: double x ND; the ASL otpical thickness, H pol
#       data.tau_v: double x ND; the ASL otpical thickness, V pol
#       data.gammah: double x ND; the ASL otpical thickness attenuation, H pol
#       data.gammav: double x ND; the ASL otpical thickness attenuation, V pol
#       data.tb_atm: double x ND; the atmosphere effective brightness temperature
#       data.tau_atm: double x ND; the atmosphere effective optical thickness
#
# Preconditions:
# * all useful dataameters in data set and ready to be used
# * assume that data fields are a mix of scalars (dimension 1) and/or
#   (vectors, matrix or more generally ND arrays) but in this latter case
#   the dimensions have to be the same for all involved arrays  (vectors,
#   matrix or ND arrays).
#   So some ND values can be equal to 1, all the remaining that are not equal to 1
#   must be same; following the MATLAB arrays operators rules when mixing
#   multi-dimensional array and scalar.
#
# Postconditions:
#
# Description:
# Compute the simulated Top of Atmosphere Brightness temperature in the
# Earth reference frame from the knwoledge of surface dataameterization and
# geomtetry. The geometry is provided through the incidences angles data.theta.
# TOA_TBH anf TOA_TBV are the vectors of modelled TOA TB in Horizontal an Vertical
# polarizations defined in the Earth reference frame
#

import numpy as np
from types import SimpleNamespace
from .soil_effective_temperature import soil_effective_temperature
from .dielectric_soil_dobson_pw import dielectric_soil_dobson_pw
from .dielectric_soil_mironov_pw import dielectric_soil_mironov_pw
from .dielectric_soil_mironov_soc import dielectric_soil_mironov_soc
from .fresnel import fresnel
from .roughness import roughness
from .tau_omega import tau_omega
from .atmosphere import atmosphere
from .toa_tb import toa_tb


def generic_fwd_model_HV(par, upf):
    # First, we need to compute the dielectric constant for the selected surface modelcst_effectivetemperature
    # Note that data should contain all the proper fields intialized for the
    # selected model i.e. for nominal kind, data.tpw_c (celcius), data.rhos
    # whereas if the selected is FWS is selected data.tsw_c is required, etc ...
    # See the associated dielectric models for details

    VER = '00g'
    model = par.model.upper()
    model_subtype = par.model_subtype.upper()
    data = SimpleNamespace() # intermediate results
    data.cos_theta = np.cos(np.deg2rad(par.theta))

    if model == 'FNO' or model == 'FNOP':
        # Rev 00d
        if par.tg_k is None:
            data.tg_k = soil_effective_temperature(par, upf)
        else:
            data.tg_k = par.tg_k
        data.tpw_c = data.tg_k - upf.zero_c_in_k # used for temperature of water in soil in dielectric
        if model_subtype == 'DOBS':
            data.epsilon = dielectric_soil_dobson_pw(data, par, upf)
        elif model_subtype == 'MIRO':
            data.epsilon = dielectric_soil_mironov_pw(data, par, upf)
        elif model_subtype == 'MIRO19':
            data.epsilon = dielectric_soil_mironov_soc(data, par, upf)
        else:
            print('\t Unknow Model Subtype {}'.format(model_subtype))

    # Second, we compute the smooth Fresnel reflectivity from the previously computed epsilon
    data.rbh, data.rbv = fresnel(data, par, upf)

    # Third, we apply roughness on data.rbh data.rbv
    # Note that for water surface proper data.qr,data.hr,data.hr_min should be set
    # to 0 from the caller.
    data.rgh, data.rgv = roughness(data, par, upf)

    # Fourth, the composite effective soil+veg temperature for Tau-Omega is
    # computed from the rough reflectivities data.rgh and data.rgv
    # Depending if we are dealing with a Low Vegetation layer or a Forest' one
    # we gave to set or override data.tg_k and data.tc_k according to:
    # for LV: compute tgc_k to replace the data.tg_k t & data.tc_k
    # for FO: use initial data.tg_k and data.tc_k are used sedataately.
    ## Rev 00e: No longer used
    # if ~strcmp(upper(model),'FFO')
    #     if ~isfield(data,'tgc_k') # If tgc_k already exists then let use it, it'has been directly provided from the caller
    #         data.tgc_k=soil_veg_effective_composite_temperature(data,upf);
    #     end
    #     data.tc_k=data.tgc_k;
    #     data.tg_k=data.tgc_k;
    # else # Forest case
    #  # Nothing to do data.tg_k is already computed at the beginning, data.tc_k
    #  # normally given from the caller.

    # Fourthth we compute ASL TBs & ASL optical thickness attenuation
    data.asl_tbh, data.asl_tbv, data.tau_h, data.tau_v, data.gamma_h, data.gamma_v, data.tau_sh, \
    data.tau_sv, data.tau_l, data.tb_soil_h, data.tb_soil_v, data.tb_veg_h, data.tb_veg_v = tau_omega(data, par, upf) ## Rev 0.0c

    # Fifth, we compute atmosphere properties if not provided.
    # It's better to compute atm properties outside and provide tb_atm tau_atm
    # in data instead. During a retrieval fwd model is called many times while
    # the atmosphere is the same so is recomputed again and again uselessly.
    if par.tb_atm is None or par.tau_atm is None:
        data.tb_atm, data.tau_atm = atmosphere(data, par, upf)
    else:
        data.tb_atm = par.tb_atm
        data.tau_atm = par.tau_atm

    # And, sixhth, we compute the final TOA TBs
    TOA_TBH, TOA_TBV = toa_tb(data, par, upf)

    return TOA_TBH, TOA_TBV
