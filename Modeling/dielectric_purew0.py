# -*- coding: utf-8 -*-

# function [epsilon_pw0 pi2rtaupw]=dielectric_purew0(par,upf)
#
# Input:
#   par: structure; contains some inputs parameters to be used for this function
#       par.tpw_c; double x ND; the pure water temperature in °C
#   upf: structure; contains the global User Parameter File external parameters
#       xxx: double x 1; many regression parameters;
# Output:
#   epsilon_pw0: double x ND; the DC dielectric constant(s) of pure water
#   pi2rtaupw: double x ND; 2 * Pi * the relaxation time of pure water (in ns)
#
# Preconditions:
# * all useful parameters in par set and ready to be used
# * assume that par fields are a mix of scalars and (vectors, matrix or
#   more generally ND) but in this latter case the dimensions have to be the
#   same for all involved arrays  (vectors, matrix or ND arrays).
#
# Postconditions:
#
# Description:
# Compute the DC dielectric constant of pure water and its relaxation time
#
# /|\ temperature in degrees celcius.
#/_!_\


def dielectric_purew0(data, par, upf):
    ow_1 = upf.ow_1
    ow_2 = upf.ow_2
    ow_3 = upf.ow_3
    ow_4 = upf.ow_4
    ow_14 = upf.ow_14
    ow_15 = upf.ow_15
    ow_16 = upf.ow_16
    ow_17 = upf.ow_17
    tpwc = data.tpw_c

    tpwc2 = tpwc * tpwc
    tpwc3 = tpwc2 * tpwc

    pi2rtaupw = ow_14 + ow_15 * tpwc + ow_16 * tpwc2 + ow_17 * tpwc3
    epsilon_pw0 = ow_1 + ow_2 * tpwc + ow_3 * tpwc2 + ow_4 * tpwc3

    return epsilon_pw0, pi2rtaupw
