# -*- coding: utf-8 -*-
"""
see LICENSE.txt file for license on the code

Contact and reference:
    Jean-Pierre Wigneron: https://doi.org/10.1016/j.rse.2020.112238,
    Xiaojun Li : https://doi.org/10.1016/j.rse.2020.112208
"""


###
# Main function for SMOS-IC algorithm
#
# 13/11/2018
###

import os
import time
import datetime
import numpy as np
from multiprocessing import Pool
from IOclasses.JobOrder import JobOrder
from IOclasses.IOlists import IOlists
from IOclasses.AuxLUTs import AuxLUTs, LCCparams
from IOclasses.AreasOfInterest import AreasOfInterest
from IOclasses.L3TBProduct import L3TBProduct
from IOclasses.AuxECMWF import AuxECMWF, ECMWFTemperatures
from IOclasses.L3SMProduct import L3SMProduct
from Utilities.optim_TBHV import optim_TBHV
from Utilities.RMSE import RMSE
from Utilities.tools import displayConfigurationPar
from Modeling.generic_fwd_model_HV import generic_fwd_model_HV

DebugMode = None
# => Classe pour tous les params : classe Loop ?
max_processes = 4   # lancement processTB
n_proc = 6 # lancement processPixel
# version = "light"  # heavy

def initArgs(dataTB_, dataECM_, dataSM_, luts_, version_):
    global dataTB, dataECM, dataSM, luts, version
    dataTB = dataTB_
    dataECM = dataECM_
    dataSM = dataSM_
    luts = luts_
    version = version_


def processPixel(args):
    par = luts.defaultPar
    upf = luts.upf
    rowsj, colsj = args

    exitFlag, dayAndTime, angles, TBH, TBV = dataTB.getValidCellData(rowsj, colsj, upf)

    # no valid data available for processing
    if exitFlag > 0:
        return exitFlag - 1

    # get ECMWF temperatures, i.e. surface/deap and 2m,
    ecmwf = dataECM.getTemps(rowsj, colsj)

    # land cover dependent data: albedo, roughness, clayFraction,
    # topoModerate, topoStrong, WetUrbanIceFraction
    lcc = LCCparams(luts.landCoverPars, luts.soilProperties,
                    luts.topography, colsj, rowsj)

    # set all processing parameters and check par validity.
    exitFlag = par.setProcessingPar(lcc, ecmwf)
    par.theta = angles
    par.sm = upf.default_init_sm[0]
    par.taus_nad = luts.tau.initialTauNad[rowsj, colsj]

    # cover type corresponding to IGBP class 12
    if exitFlag > 0:
        return 4

    # set scence flags
    exitFlag, sceneFlag = lcc.getSceneFlags(upf, ecmwf)

    # in case of frozen or polluted scene discard
    if exitFlag > 0:
        dataSM.Scene_Flags[rowsj, colsj] = sceneFlag
        if version == "light":
            return 4 + exitFlag

    if np.isnan(par.taus_nad):
        return 4

    # Inversion
    # optim_TBHV calls lsqnonlin and this calls mtb_cost_hv (through
    # funfcn{3}. Check the last one!! Inside it there is the forward model:
    # generic_fwd_model_HV
    x0 = [par.taus_nad, par.sm]  # initialisation (first guess): [taus_nad sm]
    yref = np.concatenate((TBH, TBV))
    obs_ref = np.concatenate((yref, x0))

    # for the time being uncertainties are constant and set to 4
    sigmah = np.full_like(angles, par.sigma_tbh)
    sigmav = np.full_like(angles, par.sigma_tbv)
    sigmax0 = np.array([par.sigma_taus_nad, par.sigma_sm])
    if sigmah.size == 0 or sigmav.size == 0:
        sigma = sigmax0
    else:
        sigma = np.concatenate((sigmah, sigmav, sigmax0))
    # errors
    obs_stdv = np.reciprocal(sigma)

    freeparnames = ['taus_nad', 'sm']

    # xr : retrievals (same order as freeparnames)
    # std_xr : standard of the derived parameters
    xr, std_xr, exitflag = optim_TBHV(x0, obs_ref, obs_stdv, par, upf, freeparnames)
    par.taus_nad = xr[0]
    par.sm = xr[1]
    par.taus_nad_se = std_xr[0]
    par.sm_se = std_xr[1]

    toa_tb_h, toa_tb_v = generic_fwd_model_HV(par, upf)
    ysim = np.concatenate((toa_tb_h, toa_tb_v))
    prod_rmse = RMSE(ysim, yref)

    # set the output prod values
    dataSM.Scene_Flags[rowsj, colsj] = sceneFlag
    dataSM.Soil_Moisture[rowsj, colsj] = par.sm
    dataSM.Soil_Moisture_StdError[rowsj, colsj] = par.sm_se
    dataSM.Optical_Thickness_Nad[rowsj, colsj] = par.taus_nad
    dataSM.Optical_Thickness_Nad_StdError[rowsj, colsj] = par.taus_nad_se
    dataSM.RMSE[rowsj, colsj] = prod_rmse
    dataSM.Days[rowsj, colsj] = dayAndTime[0]
    dataSM.UTC_Seconds[rowsj, colsj] = dayAndTime[1]
    dataSM.UTC_Microseconds[rowsj, colsj] = dayAndTime[2]

    return 10 if sceneFlag == 0 and prod_rmse <= upf.th_max_rmse[0] else 9


def processTBmp(args):
    """
    under development - not tested
    """
    i = args[0]
    ioListSM = args[1]
    ioListSMVOD = args[2]
    AOI = args[3]
    luts = args[4]
    version = args[5]
    processTB(i, ioListSM, ioListSMVOD, AOI, luts, version)


def processTB(i, ioListSM, ioListSMVOD, AOI, luts, version):
    tbf = ioListSM.tbflist[i]
    ecmwff = ioListSM.ecmwfflist[i]
    smf = ioListSM.smflist[i]
    smfSMVOD = ioListSMVOD.smflist[i]
    t0 = time.time()

    dataTB = L3TBProduct(tbf)  # Read L3 TB and other parameters
    dataECM = AuxECMWF(ecmwff)  # Read ECMWF data as needed
    dataSM = L3SMProduct(dataTB, dataECM)  # create L3 SM data
    luts.tau.set_init_tau(ioListSMVOD.get_window(i, luts.defaultPar.window_size),
                          ioListSMVOD.dates[i].month, luts.upf)

    pool = Pool(n_proc, initArgs, (dataTB, dataECM, dataSM, luts, version))
    chunksize = max(len(AOI) // (n_proc * 500), 1)
    results = pool.imap_unordered(processPixel, zip(AOI.rows, AOI.cols),
                                  chunksize)
    pool.close()

    count = np.zeros(11, dtype=np.uint32)
    for exitFlag in results:
        count[exitFlag] += 1

    # dataSM.generateProduct(smf)
    dataSM.generateSmProduct(smf, version)
    dataSM.generateSmVodProduct(smfSMVOD, version)
    t2 = time.time()
    elapsed = t2 - t0

    return count, elapsed


def processSMOSIC(joborder, mode="sequential"):
    # séparé to main pour permettre l'utilisation de cProfile

    if joborder is None:
        # abort
        print(" Error : unable to process with job order") #  file", jobOrderFile)
        exit(1)
    # current working directory
    sm_output_folder = joborder.OutputFolderName
    version = joborder.Version

    # read the inputs and build needed objects
    tbfname = joborder.InputL3TBlist
    ecmwffname = joborder.InputL3ECMWFlist
    smProdOutputFolder = os.path.join(sm_output_folder, 'Outputs_' + datetime.datetime.now().strftime("%Y%m%dT%H%M%S"))
    DebugMode = 0
    # VersionProc = '106'
    VersionProc = '200'
    smOnlyProdOutputFolder = os.path.join(smProdOutputFolder, 'SM')
    smVodProdOutputFolder = os.path.join(smProdOutputFolder, 'SMVOD')
    ioLists = IOlists(tbfname, ecmwffname, smProdOutputFolder, DebugMode, VersionProc)
    ioListsSM = IOlists(tbfname, ecmwffname, smOnlyProdOutputFolder, DebugMode, VersionProc)
    ioListsSMVOD = IOlists(tbfname, ecmwffname, smVodProdOutputFolder, DebugMode, VersionProc)

    # 2) set attributes
    # for every file in the L3 TB list
    luts = AuxLUTs(joborder)
    AOI = AreasOfInterest(joborder.LandSeaMaskFileName, joborder.AreaOfInterestCoordinatesFileName)

    # print("\t joborder version = ", version)
    # print('\t Input data files Ok!')
    displayConfigurationPar(joborder, AOI, luts.defaultPar)

    print('Start processing L3TB files ... ')
    print(' nb of files to process : ', len(ioListsSM.tbflist))
    print('# in list, L3 TB filename, Percent processed, Percent recommended, ' \
          'No data (start), First filter fail, Angle sep fail, Date invalid, ' \
          'Proc Par invalid, Polluted scene, Frozen scene, Elapsed time (sec)')

    # 3) loop
    if mode == "sequential":
        for i in range(len(ioLists)):
            name = os.path.basename(ioLists.tbflist[i])
            print(f"{i + 1}, {name}, ", end="")

            count, elapsed = processTB(i, ioListsSM, ioListsSMVOD, AOI, luts, version)
            count[9] += count[10]
            recommended = count[10] / count[9] if count[9] > 0 else 0.0
            count = count / len(AOI)
            print(f"{count[9]:.2%}, {recommended:.2%}, {count[0]:.2%}, " \
                  f"{count[1]:.2%}, {count[2]:.2%}, {count[3]:.2%}, " \
                  f"{count[4]:.2%}, {count[5]:.2%}, {count[6]:.2%}, {elapsed:.2f}s")
    elif mode == "parallel":
        pool = Pool(min(len(ioListsSM), max_processes))
        #results = pool.imap(processTB, [(ioLists[i], luts) for i in range(len(ioLists))])
        # results = pool.imap(processTB, [(ioLists[i], luts) for i in range(len(ioLists))])
        results = pool.imap(processTBmp, [(i, ioListsSM[i], ioListsSMVOD[i], AOI, luts, version) for i in range(len(ioLists))])
        pool.close()
        for i, (count, elapsed) in enumerate(results):
            recommended = count[10] / count[9] if count[9] > 0 else 0.0
            count = count / len(luts.AOI.rows)
            name = os.path.basename(ioLists.tbflist[i])
            print(f"{i + 1}, {name}, {count[9]:.2%}, {recommended:.2%}, " \
                  f"{count[0]:.2%}, {count[1]:.2%}, {count[2]:.2%}, {count[3]:.2%}, " \
                  f"{count[4]:.2%}, {count[5]:.2%}, {count[6]:.2%}, {elapsed:.2f}s")

    else:
        print("\t error unknown mode ", mode)


if  __name__ == "__main__":
    # 17/10 : ajouter time.time() + log (diary : écriture dans fichier ? )
    # tester profile si temps dispo
    # découper le main en fonctions
    # 1) init param
    # 2) set attributes
    # et commenter !!!
    mode = "sequential"
    Dir = os.path.dirname(os.path.realpath(__file__))
    # ASC
    jobOrderFiles = []
    
    #jobOrderAscFile = os.path.join(Dir, 'UserParameters', 'JobOrder_ASC_heavy.txt')
    # for server : 
    """
    jobOrderAscHeavyFile = os.path.join(Dir, 'UserParameters', 'JobOrder_ASC_heavy.txt')
    jobOrderDesHeavyFile = os.path.join(Dir, 'UserParameters', 'JobOrder_DES_heavy.txt')
    jobOrderAscLightFile = os.path.join(Dir, 'UserParameters', 'JobOrder_ASC_light.txt')
    jobOrderDesLightFile = os.path.join(Dir, 'UserParameters', 'JobOrder_DES_light.txt')

    jobOrderFiles.append(jobOrderAscHeavyFile)
    jobOrderFiles.append(jobOrderDesHeavyFile)
    jobOrderFiles.append(jobOrderAscLightFile)
    jobOrderFiles.append(jobOrderDesLightFile)
    """
    
    jobOrderAscFile = os.path.join(Dir, 'UserParameters', 'JobOrder_heavy.txt')
    jobOrderRE07File = os.path.join(Dir, 'UserParameters', 'JobOrder_RE07.txt')
    jobOrderOperFile = os.path.join(Dir, 'UserParameters', 'JobOrder_OPER.txt')
    jobOrderFiles.append(jobOrderAscFile)
    jobOrderFiles.append(jobOrderRE07File)
    jobOrderFiles.append(jobOrderOperFile)
    

    for jobOrderFile in jobOrderFiles:
        print("\t Processing job order file ", jobOrderFile)
        jobOrder = JobOrder(jobOrderFile, Dir)
        processSMOSIC(jobOrder, mode)
        print("\t done \n")
