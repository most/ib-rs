# -*- coding: utf-8 -*-

# fonctions utilitaires
# => Classe pour tous les params : classe Loop ?

import os
import numpy as np
from multiprocessing import RawArray
from ctypes import Array
from operator import mul
from functools import reduce


class SharedArrayManager:
    """
    Helper class to share NumPy arrays between processes.

    Pickling NumPy arrays copies and deletes RawArray buffers.
    This class saves them and reinitializes the arrays.

    Notes:
    Fork's copy-on-write can hide this issue.
    Won't be needed when multiprocessing uses Pickle 5.
    """
    def __init__(self):
        self._raw_arrays = {}

    @classmethod
    def new_array(cls, shape, dtype, fill_value=None):
        dtype = np.dtype(dtype).newbyteorder("=")
        raw_array = RawArray(dtype.char, reduce(mul, shape))
        np_array = np.ndarray(shape, dtype, raw_array)
        if fill_value is not None:
            np.copyto(np_array, fill_value, casting="unsafe")
        return np_array

    @classmethod
    def copy_array(cls, a, dtype=None):
        if dtype is None:
            dtype = a.dtype
        return cls.new_array(a.shape, dtype, a)

    def __setattr__(self, name, value):
        if isinstance(value, np.ndarray) and isinstance(value.base, Array):
            self._raw_arrays[name] = value.base
        super().__setattr__(name, value)

    def __getstate__(self):
        state = self.__dict__.copy()
        for key in self._raw_arrays:
            state[key] = state[key].shape
        return state

    def __setstate__(self, state):
        for key, value in state["_raw_arrays"].items():
            state[key] = np.ndarray(state[key], np.dtype(value._type_), value)
        self.__dict__.update(state)


def displayConfigurationPar(auxFileNames, AOI, par):
    print('\t ')
    print('\t Configuration files and parameters ...')
    print('\t -----------------------------------------------')
    print('\t Input TB List file: {}'.format(auxFileNames.InputL3TBlist))
    print('\t Input ECMWF List file: {}'.format(auxFileNames.InputL3ECMWFlist))
    print('\t Land cover fraction file: {}'.format(auxFileNames.LandCoverFractionsFileName))
    print('\t Land cover based parameter file: {}'.format(auxFileNames.LandCoverClassesParameterIGBPxlsFileName))
    print('\t Soil properties file: {}'.format(auxFileNames.SoilPropertiesFileName))
    print('\t Initial Tau file (place holder): {}'.format(auxFileNames.InitalTauFileName))
    print('\t Current Tau file (place holder): {}'.format(auxFileNames.CurrentTauFileName))
    print('\t Topography file: {}'.format(auxFileNames.TopographyFileName))
    print('\t Land Sea mask file: {}'.format(auxFileNames.LandSeaMaskFileName))
    print('\t Area of interest selection file: {}'.format(auxFileNames.AreaOfInterestCoordinatesFileName))
    print('\t Areas of interest: ')
    print('\t Name, Min Lat, Max Lat, Min Long, Max Long')
    region = AOI.region
    select = AOI.select
    latMin = AOI.latMin
    latMax = AOI.latMax
    lonMin = AOI.lonMin
    lonMax = AOI.lonMax
    for i in range(len(region)):
        if int(select[i]) == 1:
            print('\t {}, {}, {}, {}, {}'.format(region[i], latMin[i], latMax[i], lonMin[i], lonMax[i]))
            print('\t Default parameters: ....')
            print(par.__dict__)
            print('\t -----------------------------------------------')


def dumpPar(data, i, toa_tb_h, toa_tb_v, rowsj, colsj):
    outputFile = 'par_' + str(i + 1) + '.csv'
    listeParams = ['asl_tbh', 'asl_tbv', 'tau_h', 'tau_v', 'gamma_h', 'gamma_v',
                   'tau_sh', 'tau_sv', 'tau_l', 'tb_soil_h', 'tb_soil_v',
                   'tb_veg_h', 'tb_veg_v']
    sep = ";"
    Header = True
    if os.path.exists(outputFile):
        Header = False
    with open(outputFile,"a+") as parFile:
        point = str(rowsj) + "_" + str(colsj)
        if Header:
            parFile.writelines("point row - col" + sep + "parameter" + sep + "value" + "\n")

        for param in listeParams:
            value = str(data[param])
            parFile.writelines(point + sep + param + sep + value + "\n")
        parFile.writelines(point + sep + "toa_tb_h" + sep + str(toa_tb_h) + "\n")
        parFile.writelines(point + sep + "toa_tb_v" + sep + str(toa_tb_v) + "\n")


# affectation des attributs
def setAttr(dic, constants):
    for key, value in constants.__dict__.items():
        setattr(dic, key, value)
    return dic


def setAttributes(obj, listAttr):
    for const in (listAttr):
        obj = setAttr(obj, const)
    return obj
