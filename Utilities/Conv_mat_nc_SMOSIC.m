%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Programme de conversion des fichiers d'entr�e de SMOS-IC du format .mat
% au format netcdf
%
% le 27/11/2018
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

% Initial Tau
load('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\InitialTau\Best30_noSF_12_17_LVOD.mat')
nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\InitialTau\Best30_noSF_12_17_LVOD.nc','meanTau','Dimensions',{'r' 1388 'c' 584},'Format','classic');
ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\InitialTau\Best30_noSF_12_17_LVOD.nc','meanTau',meanTau);

% % Current Tau
load('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\InitialTau\Best30_noSF_12_17_LVOD_Current.mat')
nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\InitialTau\Best30_noSF_12_17_LVOD_Current.nc','currentTauNad','Dimensions',{'r' 1388 'c' 584},'Format','classic');
ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\InitialTau\Best30_noSF_12_17_LVOD_Current.nc','currentTauNad',currentTauNad);

% Soil property
% load('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.mat')
% [xlonEASE2,xlatEASE2]=meshgrid(lonEASE2,latEASE2);
% 
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','bdEASE2','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','clayEASE2','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','latEASE2','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','lonEASE2','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','sandEASE2','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% 
% 
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','bdEASE2',bdEASE2);
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','clayEASE2',clayEASE2);
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','latEASE2',fliplr(xlatEASE2'));
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','lonEASE2',xlonEASE2');
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\ClayPercentage\clay_sand_bd_EASE2.nc','sandEASE2',sandEASE2);

% Soil topography
% load('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\TopographyFractions\topographyFractionsEASE2.mat')
% 
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\TopographyFractions\topographyFractionsEASE2.nc','moderateTopography','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\TopographyFractions\topographyFractionsEASE2.nc','strongTopography','Dimensions',{'r' 1388 'c' 584},'Format','classic');
% 
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\TopographyFractions\topographyFractionsEASE2.nc','moderateTopography',moderateTopography);
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\TopographyFractions\topographyFractionsEASE2.nc','strongTopography',strongTopography);

% IGBP
% load('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\LandCoverFractions\ease2IGBP\EASE2IGBP_BroxtonFraction.mat')
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\LandCoverFractions\ease2IGBP\EASE2IGBP_BroxtonFraction.nc','EASE2IGBPFractions','Dimensions',{'x',1388,'y',584,'z',17},'Format','classic');
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\LandCoverFractions\ease2IGBP\EASE2IGBP_BroxtonFraction.nc','EASE2IGBPFractions',EASE2IGBPFractions);

% Ice
% load('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\LandCoverFractions\ease2IGBP\ice.mat')
% nccreate('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\LandCoverFractions\ease2IGBP\ice.nc','ice','Dimensions',{'x',1388,'y',584},'Format','classic');
% ncwrite('C:\Users\Fred\Documents\MATLAB\SMOS_IC_code\SMOS_INRA_CESBIO_v105_20170901\AuxData\LandCoverFractions\ease2IGBP\ice.nc','ice',ice);

