# -*- coding: utf-8 -*-

from scipy.optimize import least_squares
import numpy as np
from .mtb_cost_hv import mtb_cost_hv


def optim_TBHV(x0, ytarget, yinv_cov, par, upf, parlist):
    sol = least_squares(mtb_cost_hv, x0, method='trf', jac='3-point', ftol=1e-4, xtol=1e-4, max_nfev=1000, args=(ytarget, yinv_cov, upf, par, parlist))
    xr = sol.x
    r = sol.fun
    J = sol.jac
    exitflag = sol.status
    # r2 = r ** 2
    # resnorm = r2.sum()
    # mse = r2.sum(axis=0) / (J.shape[0] - J.shape[1])
    variance = r.var()
    try:
        Sigma = np.linalg.inv(J.conj().transpose() @ J) * variance
        se = np.sqrt(Sigma.diagonal())
    except Exception as e:
        print("\t Erreur calcul sigma : ", e )
        print("\t J = ", J)

    return xr, se, exitflag
