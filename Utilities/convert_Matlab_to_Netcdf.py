# -*- coding: utf-8 -*-

import scipy.io as sio
import h5py
# from scipy.io import netcdf

def readMatFile(matlabFile):
    """
    read matlab file v >= 7.3
    
    """
    dataDict = dict()
    with h5py.File(matlabFile, 'r') as mFile:
        for key, value in mFile.items():
            dataDict[key] = np.array(value)
            # print("\t ", key, dir(value))
            # print("\t ", key, value.name, value.fillvalue, value.dtype)
            # print(type(value))
            # print("\t", key, "\n \t\t min = ", dataDict[key].min(), "\t max = ", dataDict[key].max(), "\n\t\t", type(value))
            # print("\t", key, "\n \t\t min = ", np.min(dataDict[key]), "\t max = ", np.amax(dataDict[key]))

    return dataDict


def readOldMatFile(matlabFile):
    """
    read matlab file
    
    """
    npLoaded = sio.loadmat(matlabFile)
    """
    Returns
    mat_dictdict
    dictionary with variable names as keys, and loaded matrices as values.
    """
    return npLoaded


def convertToNc(data):
    
    print(" vars : ", data.keys())
    
    ncData = dict()
    return ncData

def convert(matlabFile, ncFile):
    # data = readMatFile(matlabFile)
    data = readOldMatFile(matlabFile)
    ncData = convertToNc(data)
    # saveToNc(ncData, ncFile)


def saveToNc(data, ncFile):
    
    # data info
    print(type(data))
    
    """
    nc = sio.netcdf.netcdf_file(ncFile, 'w')

    nc.createDimension('t', 1464)
    nc.createDimension('y', 73)
    nc.createDimension('x', 144)
    
    air_var = nc.createVariable( 'air','float32', ('t', 'y', 'x'))
    air_var[:] = air_c[:]
    nc.close()
    """
    
    
if __name__ == "__main__":
    
    matlabFile = "/donnees/Projets/TLD/SMOSIC/smos-ic-v2/AuxData/InitialTau/Each_month_initTau_median_12_19Asc_heavy.mat"
    # matlabFile = "/donnees/Projets/TLD/SMOSIC/smos-ic-v2/AuxData/InitialTau/Each_month_initTau_median_12_19Desc_heavy.mat"
    ncFile = "/tmp/initialTau.nc"
    convert(matlabFile, ncFile)
    # data1 = readMatFile(matlabFile) # NOK
    # data2 = readOldMatFile(matlabFile) # OK
    # print(data2)
