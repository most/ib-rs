# -*- coding: utf-8 -*-

import numpy as np


def RMSE(x1, x2):
    diff2 = (x1 - x2) ** 2
    y = np.sqrt(diff2.mean())
    return y
