# -*- coding: utf-8 -*-

from Modeling.generic_fwd_model_HV import generic_fwd_model_HV
import numpy as np


def mtb_cost_hv(x, ytarget, yinv_cov, upf, par, freeparname):
    for k in range(len(x)):
        setattr(par, freeparname[k], x[k])
        ## we discard the HR(SM) depenence here - to comment if necessary
        if 'hr' == freeparname[k]:
            par.hr_min = x[k]

    toa_tb_h, toa_tb_v = generic_fwd_model_HV(par, upf)
    TB = np.concatenate((toa_tb_h, toa_tb_v, x))
    ## With prior version
    #TB[np.argwhere(np.isnan(TB))] = 150
    J = (TB - ytarget) * yinv_cov
    ## No prior version: we assume that yinv_cov contains always at its end  the place for prior std on the free parameters even no used
    ## at the end
    # J=(TB(:)' - ytarget)./yinv_cov(1:end-length(x));
    return J
