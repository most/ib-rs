# -*- coding: utf-8 -*-

import math

class PhysicalConstants:
    zero_c_in_k = 273.15                # °C, Celsius2Kelvin
    k_bc = 5.67e-8                      # Watt.m^2.K^-4, Boltzman constant
    epsilon_0 = 8.854187817e-12         # F.m^-1, permittivity of vacuum
    eta_fs = 377                        # Ohm, intrinsic impedance of free space
    c = 2.99792458e8                    # m.s^-1, Speed of light in vacuum.Frequency / wavelength conversion
    epsilon_winf = 4.9                  # F.m^-1, High frequency limit of static water dielectric constant
    omega_e = 0.2506846                 # deg.minute^-1, Angular speed of the Earth


class SMOSConstants:
    f_SMOS = 1.4135e9                    # Hz, Central frequency of microwave sensor
    lambda_SMOS = 0.21                   # m, Wavelength of microwave sensor
    bd_s = 19                            # MHz, Equivalent SMOS bandwidth
    tilt = 33                            # deg, Elevation angle of antenna plane


class PreprocessingConstants:
    # Nominal, minimum and maximum values
    th_size = [55, 30, 100]               # km, Spatial resolution size requirement
    th_elon = [1.5, 1.3, 2]               # No unit, Spatial resolution elongation requirement
    c_eaf = [1, 1, 10]                    # No unit, Radiometric uncertainty multiplying factor for non AF views
    c_border = [2, 1, 10]                 # No unit, Radiometric uncertainty multiplying factor for BORDER views
    c_sun_tails = [1, 1, 10]              # No unit, Radiometric uncertainty multiplying factor for SUN_TAILS views
    c_sun_glint_area = [1, 1, 10]         # No unit, Radiometric uncertainty multiplying factor for SUN_GLINT_AREA views
    c1_rfi = [8, 0, 10]                   # No unit, Coefficient for Radiometric uncertainty factor from RFI map
    c2_rfi = [1, 0, 10]                   # No unit, Coefficient for Radiometric uncertainty factor from RFI map
    th_tbam_min = [50, 0, 100]            # K, Antenna level TB module range check must belong to [TH_TBAM_MIN, TH_TBAM_MAX]
    th_tbam_max = [500, 200, 500]         # K, Antenna level TB module range check must belong to [TH_TBAM_MIN, TH_TBAM_MAX]
    th_tbx_min = [50, 0, 100]             # K, Antenna level TBX range check must belong to [TH_TBX_MIN, TH_TBX_MAX]
    th_tbx_max = [340, 200, 500]          # K, Antenna level TBX range check must belong to [TH_TBX_MIN, TH_TBX_MAX]
    th_tby_min = [50, 0, 100]             # K, Antenna level TBY range check must belong to [TH_TBY_MIN, TH_TBY_MAX]
    th_tby_max = [340, 200, 500]          # K, Antenna level TBY range check must belong to [TH_TBY_MIN, TH_TBY_MAX]
    th_tbxy_Re_min = [-50, -100, 100]     # K, Antenna level Re TBXY range check must belong to [TH_TBXY_RE_MIN, TH_TBXY_RE_MAX]
    th_tbxy_Re_max = [50, -100, 100]      # K, Antenna level Re TBXY range check must belong to [TH_TBXY_RE_MIN, TH_TBXY_RE_MAX]
    th_tbxy_Im_min = [-50, -50, 50]       # K, Antenna level Im TBXY range check must belong to [TH_TBXY_IM_MIN, TH_TBXY_IM_MAX]
    th_tbxy_Im_max = [50, -50, 50]        # K, Antenna level Im TBXY range check must belong to [TH_TBXY_IM_MIN, TH_TBXY_IM_MAX]
    th_hom_st1 = [0.1275, 0, 0.5]         # No unit, Half stokes test application condition: sufficient homogeneous WA required
    th_min_angle = [20, 0, 60]            # No unit, The angle below which the TB will be rejected
    th_max_angle = [55, 0, 60]            # No unit, The angle above whcih the TB will be rejected
    th_min_angle_separation = [10, 0, 60] # No unit, The angle separation below which processing will not proceed
    th_std_tb = [5, 0, 20]                # No unit, The constant threshold used to reject a TB when comparing std with accuracy
    dtb_f = [2.57, 2, 8]                  # K, Scaling factor used to compute MVAL0
    cval_2 = [0.5, 0.4, 1]                # No unit, Coefficient used to compute MVAL0
    cval_4 = [0.6, 0.4, 1]                # No unit, Coefficient used to compute MVAL0
    th_mmin0 = [3, 0, 15]                 # No unit, Minimum threshold on the number of available TB after L1c pixel filtering
    th_ava_min = 20                       # No unit, Minimum number of paired views for applying RFI L2 test
    ca_tbs1 = 5                           # No unit, Coefficient for RFI L2 test
    cb_tbs1 = 4                           # No unit, Coefficient for RFI L2 test
    th_rfi_st4 = [50, 0, 300]             # K, Threshold for detecting RFI using the 4th Stokes parameter


class WEFConstants:
    WEF_SIZE = 123                        # km, Size of the squared fine grid area (in km) over which MEAN_WEF fractions, WEFfractions and reference parameter values are computed
    c_wef1 = 73.30                        # No unit, Coefficient in WEF approximation
    c_wef2 = 1.4936                       # No unit, Coefficient in WEF approximation
    c_wef3 = 524.5                        # No unit, Coefficient in WEF approximation
    c_wef4 = 2.1030                       # No unit, Coefficient in WEF approximation
    c_mwef1 = 40                          # km, Parameter in MEAN_WEF approximation
    c_mwef2 = 0.0027                      # No unit, Parameter in MEAN_WEF approximation


class AllmodelsConstants:
    tg = [288, 250, 350]                  # K, Soil effective temperature. Auxiliary ECMWF data fall back default, all models
    default_init_sm = [0.2, 0, 1]         # sm^3/sm, Default Initial SM value, all models
    default_sigma_sm = [0.2, 0, 1]        # sm^3/sm, Default sigma of SM, all models
    default_init_taus_nad = [0.5, 0, 2]   # No unit, Default initial Tau Nadir, all_models
    default_sigma_taus_nad = [0.05, 0, 2] # No unit, Default sigma of Tau Nadir, all models
    default_sigma_tbh = [4, 0, 10]        # K, Default sigma of TB_H, all models
    default_sigma_tbv = [4, 0, 10]        # K, Default sigma of TB_V, all_models
    default_window_size = [10, 5, 365]    # No unit, Default window size
    sym = [0, 0, 1]                       # No unit, Symetrization around SM=0 0=No, 1=Yes, All Soil dielectic model
    tpoly = [0, 0, 1, 2, 3]               # No unit, Symetrization type, All Soil dielectic model
    sm1 = [0.02, 0, 0.1]                  # No unit, SM Threshold for symetrizatrion, All Soil dielectic model


class DobsonmodelConstants:
    rhos = [2.664, 0.5, 3]                # g.cm^-3, Soil particle density
    rhob = [1.3, 0.5, 3]                  # g.cm^-3, standard soil specific density
    alpha = 0.65                          # No unit, Dobson model empirical coefficients
    sal = [35, 0, 50]                     # ppt, Soil salinity: be kept for future use
    cpa1 = 1.01                           # F^1/2.m^-1/2, Coefficients for computing dielectric constant of solid particles epsilon_pa
    cpa2 = 0.44                           # F^1/2.m.g^-1/2, Coefficients for computing dielectric constant of solid particles epsilon_pa
    cpa3 = -0.062                         # F.m^-1, Coefficients for computing dielectric constant of solid particles epsilon_pa
    epsilon_pa = 4.7                      # F.m^-1, Dielectric content  of solid particles
    sgef1 = -1.645                        # No unit, Coefficients for computing  sigma_eff
    sgef2 = 1.939                         # No unit, Coefficients for computing  sigma_eff
    sgef3 = -2.256                        # No unit, Coefficients for computing  sigma_eff
    sgef4 = 1.594                         # No unit, Coefficients for computing  sigma_eff
    bere1 = 1.2748                        # No unit, Coefficients for computing beta_epsilon_prime
    bere2 = -0.519                        # No unit, Coefficients for computing beta_epsilon_prime
    bere3 = -0.152                        # No unit, Coefficients for computing beta_epsilon_prime
    beim1 = 1.338                         # No unit, Coefficients for computing beta_epsilon_second
    beim2 = -0.603                        # No unit, Coefficients for computing beta_epsilon_second
    beim3 = -0.166                        # No unit, Coefficients for computing beta_epsilon_second


class MironovmodelConstants:
    permit0 = 8.854e-12                   #
    epwi0 = 4.9                           #
    nd0 = 1.62847                         #
    nd1 = -0.70803                        #
    nd2 = 0.4659                          #
    kd0 = 0.03945                         #
    kd1 = -0.03721                        #
    xmvt0 = 0.00625                       #
    xmvt1 = 0.33918                       #
    tf0 = 20                              #
    e0pb0 = 79.82918                      #
    e0pb1 = -85.36581                     #
    e0pb2 = 32.70444                      #
    bvb0 = -5.96311e-19                   #
    bvb1 = -1.25999e-3                    #
    bvb2 = 1.83991e-3                     #
    bvb3 = -9.77347e-4                    #
    bvb4 = -1.39013e-7                    #
    bsgb0 = 0.0028                        #
    bsgb1 = 2.37388e-2                    #
    bsgb2 = -2.93876e-2                   #
    bsgb3 = 3.28954e-2                    #
    bsgb4 = -2.00582e-2                   #
    dhbr0 = 1466.80741                    #
    dhbr1 = 26.97032e2                    #
    dhbr2 = -0.09803e4                    #
    dsrb0 = 0.88775                       #
    dsrb1 = 0.09697e2                     #
    dsrb2 = -4.2622                       #
    taub0 = 48e-12                        #
    sbt0 = 0.29721                        #
    sbt1 = 0.49                           #
    e0pu = 100.0                          #
    bvu0 = 1.10511e-4                     #
    bvu1 = -5.16834e-6                    #
    bsgu0 = 0.00277                       #
    bsgu1 = 3.58315e-2                    #
    dhur0 = 2230.20237                    #
    dhur1 = -0.39234e2                    #
    dsur0 = 3.6439                        #
    dsur1 = -0.00134e2                    #
    tauu0 = 48e-12                        #
    sut0 = 0.12799                        #
    sut1 = 1.65164                        #


class EffectivetemperaturemodelConstants:
    w0 = 0.3                              # m^3/m^3, w0 and bw0 are used to obtain the weighting coeff Ct for computing Ts. Those parameters depend mainly on the soil texture and structure. 
    bw0 = 0.3                             # They are superseded by values in the DFFG_SOIL_PROPERTIES table when available


class DielectricwatermodelConstants:
    sst = [288, 260, 320]                 # K, Water temperature (pure or saline). Fall back default of forecast SST
    sss = [35, 0, 40]                     # ppt, Water salinity (saline water)
    ow_1 = 88.045                         # No unit, Klein and Swift
    ow_2 = -0.4147                        # No unit, Klein and Swift
    ow_3 = 6.295e-4                       # No unit, Klein and Swift
    ow_4 = 1.075e-5                       # No unit, Klein and Swift
    ow_5 = 87.134                         # No unit, Klein and Swift
    ow_6 = -1.949e-1                      # No unit, Klein and Swift
    ow_7 = -1.276e-2                      # No unit, Klein and Swift
    ow_8 = 2.491e-4                       # No unit, Klein and Swift
    ow_9 = 1.0                            # No unit, Klein and Swift
    ow_10 = 1.613e-5                      # No unit, Klein and Swift
    ow_11 = -3.656e-3                     # No unit, Klein and Swift
    ow_12 = 3.210e-5                      # No unit, Klein and Swift
    ow_13 = -4.232e-7                     # No unit, Klein and Swift
    ow_14 = 1.1109e-10                    # No unit, Klein and Swift
    ow_15 = -3.824e-12                    # No unit, Stogryn
    ow_16 = 6.938e-14                     # No unit, Stogryn
    ow_17 = -5.096e-16                    # No unit, Stogryn
    ow_18 = 1.0                           # No unit, Klein and Swift
    ow_19 = 2.282e-5                      # No unit, Klein and Swift
    ow_20 = -7.638e-4                     # No unit, Klein and Swift
    ow_21 = -7.760e-6                     # No unit, Klein and Swift
    ow_22 = 1.105e-8                      # No unit, Klein and Swift
    ow_23 = 0.18252                       # No unit, Weyl & Stogryn
    ow_24 = -1.4619e-3                    # No unit, Weyl & Stogryn
    ow_25 = 2.093e-5                      # No unit, Weyl & Stogryn
    ow_26 = -1.282e-7                     # No unit, Weyl & Stogryn
    ow_27 = 2.033e-2                      # No unit, Weyl & Stogryn
    ow_28 = 1.266e-4                      # No unit, Weyl & Stogryn
    ow_29 = 2.464e-6                      # No unit, Weyl & Stogryn
    ow_30 = 1.849e-5                      # No unit, Weyl & Stogryn
    ow_31 = -2.551e-7                     # No unit, Weyl & Stogryn
    ow_32 = 2.551e-8                      # No unit, Weyl & Stogryn


class CardoidmodelConstants:
    u_card = [0, 0, math.pi]              # rd, Angle parameter
    b_card = 0.8                          # F.m^-1, A constant for cardioid model


class DielectricmodelConstants:
    epsilon_sand = 2.53 - 0.05j           # F.m^-1, Dielectric constant for dry sand at L band
    epsilon_frz = 5 - 0.5j                # F.m^-1, Dielectric constant for frozen soil
    epsilon_ice = 3.17 - 0.1j             # F.m^-1, Dielectric constant for ice. epsi" is very small for pure ice Currently suggested eps_ice"= 0.1
    epsilon_urban = 5.7 - 0.074j          # F.m^-1, Dielectric constant for urban area
    epsilon_rock = [5.7 - 0.074j, 2.4 + 0.074j, 9.6 + 0.074j] # F.m^-1, Dielectric constant for barren areas
    sand_frac = [0.4, 0, 0.9]             # No unit, Default sand fraction when needed for dielectric model
    clay_frac = [0.2, 0, 0.9]             # No unit, Default clay fraction when needed for dielectric model
    soil_salinity = [0.65, 0, 0.9]        # No unit, Default soil salinity when needed for dielectric model


class FresnelmodelConstants:
    mus = 1.0                              # H.m^-1, Soil magnetic permeability
    muw = [1.0, 0.99, 1.001]               # H.m^-1,'Water magnetic permeability (not equal to 1'}


class SurfaceroughnessConstants:
    cwp1 = 0.06774                         # No unit, Coefficient for cmputing roughnessHR(SM) as a piecewise function of SM
    cwp2 = -0.00064                        # No unit, Coefficient for cmputing roughnessHR(SM) as a piecewise function of SM
    cxmvt1 = 0.49                          # No unit, Coefficient for cmputing roughnessHR(SM) as a piecewise function of SM
    cxmvt2 = 0.165                         # No unit, Coefficient for cmputing roughnessHR(SM) as a piecewise function of SM
    xmvt = [0, 0, 0.9]                     # No unit, Default xmvt
    fc = [0.3, 0, 0.9]                     # No unit, Default field capacity


class SnowmodelConstants:
    epsilon_snow = [1.5 - 0.026j, 1 - 0.006j, 3.4 - 0.08j]    # F.m^-1, It will be required by computing the TB contribution from Snow fraction
    scr = 0.015                           # m, Minimum snow mass that ensures complete coverage of an ECMWF grid box. Use to compute snow fraction


class SkymodelConstants:
    tb_sky_h = [0, 0, 4]                  # No unit, Default TB of Sky for H
    tb_sky_v = [0, 0, 4]                  # No unit, Default TB of Sky for V


class VegetationmodelConstants:
    sm_litter = [0.3, 0, 0.5]             # m^3.m^-3, Soil Moisture to derive optical thickness of litter when soil+low veg is not regressed but used as default contribution. Fall back default to ECMWF SWVL unavailability
    tc_lv = [288, 250, 350]               # K, Effective vegetation temperature.Fall back default to ECMWF SKT,STL and SM unavailability
    tc_fv = [288, 250, 350]               # K, Effective vegetation temperature.Fall back default to ECMWF SKT,STL and SM unavailability
    mgl_min = [0, 0, 1]                   # No unit, minimum value for Mg_L in Tau_L i.e. if Mg_L<mgl_min then Mg_L=mgl_min
    mgl_max = [0.8, 0, 1]                 # No unit, maximum value for Mg_L in Tau_L i.e. if Mg_L>mgl_max then Mg_L=mgl_max
    tgc_use = [False, True, False]        # Tgc use (true) or not use (false) in MNO modelling


class AtmospheremodelConstants:
    tb_atm = [0, 0, 4]                     # No unit, Default TB of atmosphere
    tau_atm = [0, 0, 4]                    # No unit, Default TB of atmosphere
    t0 = [288, 230, 310]                   # K, Temperature at 2 meters. Fall back default to ECMWF 2T unavailability
    p0 = [1013, 400, 1100]                 # hPa, Surface pressure Fall back default to ECMWF SP unavailability
    wvc = [1, 0, 10]                       # kg.m^-2, Total water vapor content.Fall back default to ECMWF SP unavailability
    k0_tau_o2 = 5.12341e3                  # neper, Oxygen optical thickness parameters fit
    kt0_tau_o2 = -6.80605e1                # neper.K^-1, Oxygen optical thickness parameters fit
    kp0_tau_o2 = 2.42216e1                 # neper.hPa^-1, Oxygen optical thickness parameters fit
    kt02_tau_o2 = 1.70616e-1               # neper.K^-2, Oxygen optical thickness parameters fit
    kp02_tau_o2 = 6.64682e-3               # neper.hPa^-2, Oxygen optical thickness parameters fit
    kt0p0_tau_o2 = -7.99404e-2             # neper.K^-1.hPa^-1, Oxygen optical thickness parameters fit
    k0_tau_h2o = -1.13724e+2               # neper, H2O optical thickness parameters fit
    k1_tau_h2o = 1.55378e-1                # neper.Hpa^-1, H2O optical thickness parameters fit
    k2_tau_h2o = 2.87254e0                 # neper.m^2.kg^-1, H2O optical thickness parameters fit
    k0_dt_o2 = -3.16387e0                  # K, Oxygen temperature contribution parameters fit
    kt0_dt_o2 = 1.38628e-1                 # No unit, Oxygen temperature contribution parameters fit
    kp0_dt_o2 = 3.29731e-3                 # K.hPa^-1, Oxygen temperature contribution parameters fit
    kt02_dt_o2 = -1.19886e-4               # K^-1, Oxygen temperature contribution parameters fit
    kp02_dt_o2 = 1.66366e-6                # K.hPa^-2, Oxygen temperature contribution parameters fit
    kt0p0_dt_o2 = -9.90743e-6              # hPa^-1, Oxygen temperature contribution parameters fit
    k0_dt_h2o = 8.07567e0                  # K, H2O temperature contribution parameters fit
    k1_dt_h2o = 5.16901e-4                 # K.hPa^-1, H2O temperature contribution parameters fit
    k2_dt_h2o = 3.44319e-2                 # K.m^2.kg^-1, H2O temperature contribution parameters fit


class PostprocessingConstants:
    sm_min = 0                             # m^3.m^-3, Soil moisture retrieval domain
    sm_max = 0.6                           # m^3.m^-3, Soil moisture retrieval domain
    th_dqx_sm = 0.1                        # m^3.m^-3, Threshold for maximum acceptable DQX on SM
    a_card_min = 0                         # No unit, Cardioid dielectric constant retrieval domain
    a_card_max =  90                       # No unit, Cardioid dielectric constant retrieval domain
    th_dqx_a_card = 10                     # No unit, Threshold for maximum acceptable DQX on cardioid dielectric constant
    t_phys_min = 230                       # K, Effective temperature retrieval domain
    smt_phys_max = 320                     # K, Effective temperature retrieval domain
    th_dqx_t_phys = 3                      # K, Threshold for maximum acceptable DQX on effective temperature
    hr_min = 0                             # No unit, Hsoil retrieval domain
    hr_max = 3                             # No unit, Hsoil retrieval domain
    th_dqx_hr = 0.1                        # No unit, Threshold for maximum acceptable DQX on Hsoil retrieval
    taus_nad_min = 0                       # neper, Tau nadir retrieval domain
    taus_nad_max = 2                       # neper, Tau nadir retrieval domain
    th_dqx_taus_nad = 0.2                  # neper, Threshold for maximum acceptable DQX on Tau nadir retrieval
    tth_min = 0                            # No unit, TTh retrieval domain
    tth_max = 6                            # No unit, TTh retrieval domain
    th_dqx_tth = 0.5                       # No unit, Threshold for maximum acceptable DQX on TTh retrieval
    rtt_min = 0                            # No unit, RTT retrieval domain
    rtt_max = 10                           # No unit, RTT retrieval domain
    th_dqx_rtt = 1                         # No unit, Threshold for maximum acceptable DQX on RTT retrieval
    diff_omega_min = -0.2                  # No unit, Diff_Omega retrieval domain
    diff_omega_max = 0.2                   # No unit, Diff_Omega retrieval domain
    th_dqx_diff_omega = 0.1                # No unit, Threshold for maximum acceptable DQX on Diff_Omega retrieval
    th_fit = 4                             # No unit, Threshold for detecting outlier
    th_scene_feb = [0.05, 0, 1]            # No unit, Presence of rocks
    th_scene_fts = [0.05, 0, 1]            # No unit, Presence of strong topography
    th_scene_ftm = [0.1, 0, 1]             # No unit, Presence of moderate topography
    th_scene_fwo = [0.05, 0, 1]            # No unit, Presence of open water
    th_scene_fsn = [0.05, 0, 1]            # No unit, Presence of snow
    th_scene_ffo = [0.1,0, 1]              # No unit, Presence of forest
    th_scene_tau_fo = [1, 0, 3]            # neper, Large forest optical thickness
    th_scene_fno = [0.1, 0, 1]             # No unit, Presence of nominal soil
    th_scene_frz = [0.05, 0, 1]            # No unit, Presence of frost
    th_scene_fwl = [0.05, 0, 1]            # No unit, Presence of wetlands
    th_scene_ful = [0.1, 0, 1]             # No unit, Presence limited urban area
    th_scene_fuh = [0.3, 0, 1]             # No unit, Presence of large urban area
    th_scene_fti = [0.05, 0, 1]            # No unit, Presence of permanent ice/snow
    th_scene_sand = [0.95, 0, 1]           # No unit, Presence of high sand fraction
    th_scene_wetUrIce = [0.1, 0, 1]        # No unit, Presence of combined water, urban, and permanent ice/snow
    th_tec = [10, 0, 100]                  # TECu, Presence significative TEC
    th_rain = [10, 0, 100]                 # mm/h, Rain intensity threshold for rain flag
    th_flood = [20, 0, 100]                # mm/h, Rain intensity threshold for flood flag
    th_dry_snow = [0.1, 0, 1]              # No unit, Threshold for dry snow
    th_tau_litter = [0.1, 0, 1]            # neper, Threshold for litter opacity flag
    th_pr = [0.026, 0, 0.04]               # No unit, Threshold vegetation interception event
    th_intercep = [0.02, 0, 0.5]           # m, ECMWF interception
    th_sea_ice = [0.2, 0, 1]               # No unit, Sea-ice fraction detection threshold
    th_chi_2_p_min = [0.5, 0, 1]           # No unit, Threshold min for chi2 interpretation
    th_chi_2_p_max = [0.95, 0, 1]          # No unit, Threshold max for chi2 interpretation
    th_max_rmse = [6, 0, 20]               # No unit, Threshold max rmse beyond which product is not recommended
    th_sky = [5, 0, 100]                   # K, Threshold for sky contribution
    theta_b = [42.5,0, 60]                 # °, Angle to generate modelled ASL brightness temperature for User Data Product
    pr_inci = [42.5, 0, 60]                # °, Angle to generate modelled ASL brightness temperature for computing vegetation interception PR index
    gg_column_max = 1600                   # No unit, Number of columns in Gaussian grid
    gg_lat_step_size = 0.225               # °, Latitude step size for Gaussian grid
    gg_lat_stop = 90                       # °, Latitude stop for Gaussian grid
    gg_long_start = -180                   # °, Longitude start for Gaussian grid
    gg_long_step_size = 0.225              # °, Longitude step size for Gaussian grid
    gg_row_max = 800                       # No unit, Number of rows in Gaussian grid


class retrieval:
    sm = [0.15, 0, .60]                      # m^3.m^-3, Soil moisture prior value. ECMWF fallback for SWL1 values
    dp_sm = [0.001, 0, .50]                  # No unit, Soil moisture increment for computing derivatives (DPD)
    a_card = [20, 0, 50]                     # F.m^-1, Default cardioid magnitude prior value. To be used with MDd retrieval
    dp_a_card = [0.01, 0, 50]                # F.m^-1, Cardioid magnitude increment for computing derivatives (DPD)
    dp_taus_nad = [0.01, 0, 0.1]             # neper, Tau nadir increment for computing derivatives (DPD)
    t_phys = [288, 260, 300]                 # K, Surface physical temperature prior value. ECMWF fallback for STL1/SM/SKT values
    dp_t_phys = [0.1, 0, 50]                 # K, T Phys increment for computing derivatives (DPD)
    dp_tt_h = [0.05, 0, 0.5]                 # No unit, tt_h increment for computing derivatives (DPD)
    dp_rtt = [0.05, 0, 0.5]                  # No unit, Rtt increment for computing derivatives (DPD)
    dp_omega_h = [0.1, 0, 0.5]               # No unit, omega_h increment for computing derivatives (DPD)
    dp_diff_omega = [0.05, 0, 0.5]           # No unit, diff_omega increment for computing derivatives (DPD)
    dp_hr = [ 0.01, 0, 0.5]                  # No unit, hr increment for computing derivatives (DPD)


# 'c0_gst0',{100.46062,[],'','Ephemeris of Greenwich Sidereal Time Origin (00:00 UTC). Polynomial approximation:GST0=C0_GST0+C1_GST0xU0+C2_GST0xU02+C4_GST0xU03'},'galactic_parameter'
# 'c1_gst0',{36000.77,[],'','Ephemeris of Greenwich Sidereal Time Origin (00:00 UTC). Polynomial approximation:GST0=C0_GST0+C1_GST0xU0+C2_GST0xU02+C4_GST0xU03'},'galactic_parameter'
# 'c2_gst0',{0.000388,[],'','Ephemeris of Greenwich Sidereal Time Origin (00:00 UTC). Polynomial approximation:GST0=C0_GST0+C1_GST0xU0+C2_GST0xU02+C4_GST0xU03'},'galactic_parameter'
# 'c4_gst0',{-2.6e-8,[],'','Ephemeris of Greenwich Sidereal Time Origin (00:00 UTC). Polynomial approximation:GST0=C0_GST0+C1_GST0xU0+C2_GST0xU02+C4_GST0xU03'},'galactic_parameter'

# 'th_t_dry',{-12+273.15,[],'K','Threshold for dry NPE snow'},'decision_tree1'
# 'th_t_wet',{-2+273.15,[],'K','Threshold for wet NPE snow'},'decision_tree1'
# 'th_sand',{.97,[.95 .97],'','Threshold for sand'},'decision_tree1'

# 'th_pwater_frz',{271,[270 273.15],'K','Threshold for frozen pure water'},'fractions_npe_update'
# 'th_swater_frz',{270,[270 273.15],'K','Threshold for frozen saline water'},'fractions_npe_update'
# 'th_soil_frz',{270,[268 274],'K','Threshold for frozen frozen soil'},'fractions_npe_update'
# 'th_tau_f1',{0.05,[0 1],'neper','Threshold for canopy opacity of (1-FFO) fraction to Obtaining the final aggregated radiometric fractions for WADFFG'},'fractions_npe_update'
# 'th_f1',{.61,[],'','Threshold for FFO fraction to deal with the Forest Winter case. Previously, it was TH_FF. '},'fractions_npe_update'
# 'th_veg_frz',{269,[265 273.15],'K','Threshold for frozen vegetation'},'fractions_npe_update'

# 'hr_frz',{0,[0 5],'','HR value for roughness of frozen soil'},'model_param_npe_update'
# 'hr_wet_mixed_snow',{0,[0 5],'','HR value for wet/mixed snow covered soil'},'model_param_npe_update'


# 'th_f2',{.60,[],'','From decision Tree threshold for FFO fraction'},'fm0_npe_update'
# 'th_no',{.40,[],'','From decision Tree threshold for FNO fraction'},'fm0_npe_update'

# 'th_mmin1',{3,[1 60],'','no retrieval-Minimun number of retrieved parameters'},'decision_tree2'
# 'th_mmin2',{30,[1 60],'','Minimun number of retrieved parameters - Nominal  number of retrieved parameters'},'decision_tree2'
# 'th_mmin3',{45,[1 60],'','Nominal  number of retrieved parameters - Maximum number of retrieved parameters'},'decision_tree2'
# 'th_23',{0.01,[0.0001 0.1],'neper','TAU_R threshold for selecting prior standard deviation values on free parameters'},'decision_tree2'
# 'th_34',{0.5,[0.4 1.5],'neper','TAU_R threshold for selecting prior standard deviation values on free parameters'},'decision_tree2'
# 'th_lag_hr',{10,[5 20],'day','Maximum delay for using current HR map'},'decision_tree2'
# 'th_lag_tau_lv',{10,[5 20],'day','Maximum delay for using current TAU_LV map'},'decision_tree2'

# 'cqx11',{1,[0.25 4],'','Radiom  delta_TB & prior'},'overall_quality'
# 'cqx21',{1,[0.1 20],'K','Instrument'},'overall_quality'
# 'cqx22',{0,[0 1],'K.km^-1','Instrument X_SWATH term'},'overall_quality'
# 'cqx23',{1,[0.5 2],'K','Calibration'},'overall_quality'
# 'cqx24',{0.2,[0 2],'K','Reconstruction overall bias'},'overall_quality'
# 'cqx25',{0.1,[0 2],'K','Reconstruction Coast line flag'},'overall_quality'
# 'cqx26',{0,[0 100],'','Reconstruction Corbella term'},'overall_quality'
# 'cqx31',{0.05,[0.01 0.2],'K','Goodness of fit'},'overall_quality'
# 'cqx32',{10,[2 50],'K','Outliers'},'overall_quality'
# 'cqx33',{0.5,[0 5],'K','Sun in front'},'overall_quality'
# 'cqx34',{0.2,[0 5],'K','Rain'},'overall_quality'
# 'cqx35',{0.2,[0 5],'K','TEC'},'overall_quality'
# 'cqx36',{0.2,[0 5],'K','Sky'},'overall_quality'
# 'cqx41',{20,[0 100],'K','Default fractions'},'overall_quality'
# 'cqx42',{5,[0 100],'K','FNO reference values'},'overall_quality'
# 'cqx43',{0,[0 100],'K','Litter'},'overall_quality'
# 'cqx44',{1,[0 10],'K','Interception'},'overall_quality'
# 'cqx45',{0.5,[0 10],'K','Interception (aux)'},'overall_quality'
# 'cqx46',{25,[0 100],'K','Flood probability'},'overall_quality'
# 'cqx47',{6,[0 200],'K','Moderate topography'},'overall_quality'
# 'cqx48',{20,[0 200],'K','Strong topography'},'overall_quality'
# 'cqx49',{20,[0 200],'K','Evening orbit'},'overall_quality'

# 'ccx0',{1,[0.2 5],'','First coefficient'},'overall_quality'
# 'ccx1',{1.78808e-1,[-1 1],'%.K^-1','A constant'},'overall_quality'
# 'ccx2',{1.22025e-2,[-1 1],'K^-1','SM factor'},'overall_quality'
# 'ccx3',{4.22671e-1,[-1 1],'%K^-1','Tau factor'},'overall_quality'
# 'ccx4',{-3.58501e-5,[-1 1],'%^-1.K^-1','SM^2 factor'},'overall_quality'
# 'ccx5',{2.24970e-1,[-1 1],'%.K^-2','TAU^2 factor'},'overall_quality'
# 'ccx6',{3.18095e-00,[-1 1],'K^-1','SM*Tau factor'},'overall_quality'

# 'use_deflt_rfi',{1,{0,1},'','Switch controlling which map is used for RFI map:1 means original static map;0 means evolving map.'},'overall_processing'
# 'use_deflt_taus_nad_lv',{1,{0,1},'','Switch controlling which maps are used for optical thickness Tau for Low Vegetation cover:1 means tau is derived only from auxiliary surface parameters;0 means tau is derived from both auxiliary surface parameters and evolving Tau map;'},'overall_processing'
# 'use_deflt_taus_nad_fo',{1,{0,1},'','Switch controlling which maps are used for optical thickness Tau for Forest cover:1 means tau is derived only from auxiliary surface parameters;0 means tau is derived from both auxiliary surface parameters and evolving Tau map.'},'overall_processing'
# 'use_deflt_hr',{1,{0,1},'','Switch controlling which maps are used for roughness parameter  HR:1 means HR is only derived from auxiliary surface parameters;0 means HR is derived from both auxiliary surface parameters and evolving Tau map.'},'overall_processing'
# 'th_cur_hr_val_period',{30,[0 30],'days','Controls maximum period of validity for HR entries in DGG_CURRENT_ROUGHNESS_H LUT'},'overall_processing'
# 'th_current_rfi_v',{1,{0,1},'','Threshold for current vertical RFI. If NV_RFI/N_SNAP > TH_CURRENT_RFI_V then FL_RFI_PRONE_V = 1,else, FL_RFI_PRONE_V = 0'},'overall_processing'
# 'th_current_rfi_H',{1,{0,1},'','Threshold for current horizontal RFI.If NH_RFI/N_SNAP > TH_CURRENT_RFI_H then FL_RFI_PRONE_H = 1, else, FL_RFI_PRONE_H = 0.'},'overall_processing'
# 'missing_val',{-999,[],'','Missing value for DGG Current LUTs'},'overall_processing'
