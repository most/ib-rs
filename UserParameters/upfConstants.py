# -*- coding: utf-8 -*-

class upfConstants:
    def __init__(self, constants_list):
        for constants in constants_list:
            # deep copy for the pickle protocol
            for key, value in constants.__dict__.items():
                if not key.startswith("_"):
                    setattr(self, key, value)
