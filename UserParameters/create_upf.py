# -*- coding: utf-8 -*-

# function create_upf([opt])
#
# Input:
#   opt: char string; OPTIONAL; if provided, control the output format of the output upf structure
#
# Output:
#   upf: cells ; contains pairwise kind structure between TGRD UPF
#   keywords and associated informations. The organisation depends of opt
#   value see the description below.
#
# Preconditions:
#   None
# Postconditions:
#   None
# Description:
# All the information contained in upf comes from TGRD (last release
# SO-TN-ARR-L2PP-0005_TGRD_v0.4_060801.doc)
#
# if opt is not provided or empty
#  upf output is a cell array that contains the following fields:
#  name,{default value,range [min max],'units','comments'},'section'
#
#   name: string; is a keyword in TGRD and the rest of fields are its associated values.
#   default value: vector; the default value of the TGRD UPF keyword named 'name'
#   range: vector; if meaningful [min max] range for the UPF keyword
#   units: string; if meaningful the units of the UPF keyword
#   comments: string; a short descriptive comments of the UPF keyword
#   section: string; the section of TGRD where the UPF keyword is defined.
#
# if opt=='struct_all'
#  upf is converted in a structure using name as the field selector and the
#  value associated is the rest of the line (except the section).
#  i.e. 'name',{default value,range [min max],'units','comments'},'section'
#  is translated into upf.name with upf.name={default value,range [min  max],'units','comments'}
#
# if opt=='struct_val'
#  upf is converted in a structure using name as the field selector and the
#  value associated is the default value.
#
# See also CAPITALIZE_THE_NAMES
#
# Authors: Philippe Richaume <philippe.richaume@cesbio.cnes.fr>
# Creation Date:  2006/04/10
# Revision: 00.0a Date: 2006/04/10
# Comments: initial creation from SO-TN-ARR-L2PP-0005 TGRD_060123.pdf (CDR release)
# Revision: 00.01 Date: 2006/07/20
# Comments: verification and adds from SO-TN-ARR-L2PP-0005_TGRD_v0.4_060801.doc (PreQR release)
# Revision: 00.02 Date: 2006/08/14
# Comments: adds many missing fields from SO-TN-ARR-L2PP-0005_TGRD_v0.4_060801.doc (PreQR release)
# Revision: 00.03 Date: 2006/08/25
# Comments: change of all values defined in % (i.e. [0 100]) to fractions (i.e [0 1])
# Revision: 00.04 Date: 2006/09/12
# Comments: fix typo for ow15 change old -3.834e-12 to new -3.824e-12
# Revision: 00.05 Date: 2006/10/15
# Comments: added version tracking field in upf struct
# Revision: 00.05 Date: 2006/10/16
# Comments: change the sign of imaginary part of epsilon_xxx to be negative'
# Revision: 00.06 Date: 2006/12/05
# Comments: Updated values for Atmosphere Coefficient and SMOS radiometer frequency based on PHW email 2006/11/30 with TGR_PW_modif 061130.doc
#           Added mgl_min & mgl_max field to control the validity domain of Mg_L in Tau Litter computation
# Revision: 00.06 Date: 2007/01/04
# Comments: Added fields dp_<parname>: the increments values to use for finite difference for derivatives computation.
# Revision: 00.06 Date: 2007/02/06
# Comments: minor change, tau_nad renamed to taus_nad to remind it means Standing vegetation Tau Nadir'
# Revision: 00.07 Date: 2008/09/16
# Comments: salinity was 2 (wrong) => changed to 35
# Revision: 00.08 Date: 2008/09/18
# Comments: 'tgc_use' field is introduced to switch on/off the use of Tgc for FNO fraction - default is false (off)
# Revision: 00.09 Date: 2009/04/21
# Comments: 'hr_frz' field is introduced to set the roughness parameters to use for HR_MAX and HR_MIN for frozen soils
#           'hr_wet_mixed_snow' is introduced to set the roughness parameters to use for HR_MAX and HR_MIN for wet/mixed snow surface
# Revision: 00.09 Date: 2009/05/01
# Comments: 'th_size' field change from 50 km to 55 km.

import math


def create_upf(opt):
    version = '2006/04/10: intial creation from SO-TN-ARR-L2PP-0005 TGRD_060123.pdf (CDR release)\n \
               2006/07/20: verification and adds from SO-TN-ARR-L2PP-0005_TGRD_v0.4_060801.doc (PreQR release)\n \
               2006/08/14: adds many missing fields from SO-TN-ARR-L2PP-0005_TGRD_v0.4_060801.doc (PreQR release)\n \
               2006/08/25: change of all values defined in % (i.e. [0 100]) to fractions (i.e [0 1])\n \
               2006/09/12: fix wrong TH_F1 value to 0.61\n \
               2006/10/02: fix typo for ow15 change old -3.834e-12 to new -3.824e-12\n \
               2006/10/15: added this version tracking field\n \
               2006/10/16: change the sign of imaginary part of epsilon_xxx to be negative\n \
               2006/12/05: Updated values for Atmosphere Coefficient and SMOS radiometer frequency based on PHW email 2006/11/30 with TGR_PW_modif 061130.doc\n \
               2007/01/04: Added fields dp_<parname>: the increments values to use for finite difference for derivatives computation\n \
               2007/01/04: modified various fields names tau_nadir into taus_nadir to remind it means Standing vegetation Tau Nadir\n \
               2008/09/18: added tgc_use boolean field to switch on (true) / off (false) tgc use for FNO\n \
               2009/04/21: added hr_frz, hr_wet_mixed_snow to set the roughness parameters to use for HR_MAX and HR_MIN for frozen soils, wet/mixed snow covered soil\n \
               2009/05/01: th_size changed from 50km to 55km\n \
               2013/06/14: added Mironov parameters'

    upf = [['version', version, [], [], [], 'Date & modif of the upf'], \
           ['pi', math.pi, [], [], 'PI', 'physical_constants'], \
           ['zero_c_in_k', 273.15, [], [], 'Celsius2Kelvin','physical_constants'], \
           ['k_bc', 5.67e-8, [], 'Watt.m^2.K^-4', 'Boltzman constant', 'physical_constants'], \
           ['epsilon_0', 8.854187817e-12, [], 'F.m^-1', 'permittivity of vaccum', 'physical_constants'], \
           ['eta_fs', 377, [], 'ohm', 'intrinsic impedance of free space', 'physical_constants'], \
           ['c', 2.99792458e8, [], 'm.s^-1', 'Speed of light in vacuum.Frequency / wavelength conversion', 'physical_constants'], \
           ['epsilon_winf', 4.9, [], 'F.m^-1', 'High frequency limit of static water dielectric constant', 'physical_constants'], \
           ['omega_e', 0.2506846, [], 'deg.minute^-1', 'Angular speed of the Earth', 'physical_constants'], \

           ['f', 1.4135e9, [], 'Hz', 'Central frequency of microwave sensor', 'smos_constants'], \
           ['lambda', 0.21, [], 'm', 'Wavelength of microwave sensor', 'smos_constants'], \
           ['bd_s', 19, [], 'MHz', 'Equivalent SMOS bandwidth', 'smos_constants'], \
           ['tilt', 33, [], 'deg', 'Elevation angle of antenna plane', 'smos_constants'], \

           ['th_size', 55, [30, 100], 'km', 'Spatial resolution size requirement', 'preprocessing'], \
           ['th_elon', 1.5, [1.3, 2], '', 'Spatial resolution elongation requirement', 'preprocessing'], \
           ['c_eaf', 1, [1, 10], '', 'Radiometric uncertainty multiplying factor for non AF views', 'preprocessing'], \
           ['c_border', 2, [1, 10], '', 'Radiometric uncertainty multiplying factor for BORDER views', 'preprocessing'], \
           ['c_sun_tails', 1, [1, 10], '', 'Radiometric uncertainty multiplying factor for SUN_TAILS views', 'preprocessing'], \
           ['c_sun_glint_area', 1, [1, 10], '', 'Radiometric uncertainty multiplying factor for SUN_GLINT_AREA views', 'preprocessing'], \
           ['c1_rfi', 8, [0, 10], '', 'Coefficient for Radiometric uncertainty factor from RFI map', 'preprocessing'], \
           ['c2_rfi', 1, [0, 10], '', 'Coefficient for Radiometric uncertainty factor from RFI map', 'preprocessing'], \

           ['th_tbam_min', 50, [0, 100], 'K', 'Antenna level TB module range check must belong to [TH_TBAM_MIN, TH_TBAM_MAX]', 'preprocessing'], \
           ['th_tbam_max', 500, [200, 500], 'K', 'Antenna level TB module range check must belong to [TH_TBAM_MIN, TH_TBAM_MAX]', 'preprocessing'], \
           ['th_tbx_min', 50, [0, 100], 'K', 'Antenna level TBX range check must belong to [TH_TBX_MIN, TH_TBX_MAX]', 'preprocessing'], \
           ['th_tbx_max', 340, [200, 500], 'K', 'Antenna level TBX range check must belong to [TH_TBX_MIN, TH_TBX_MAX]', 'preprocessing'], \
           ['th_tby_min', 50, [0, 100], 'K', 'Antenna level TBY range check must belong to [TH_TBY_MIN, TH_TBY_MAX]', 'preprocessing'], \
           ['th_tby_max', 340, [200, 500], 'K', 'Antenna level TBY range check must belong to [TH_TBY_MIN, TH_TBY_MAX]', 'preprocessing'], \

           ['th_tbxy_Re_min', -50, [-100, 100], 'K', 'Antenna level Re TBXY range check must belong to [TH_TBXY_RE_MIN, TH_TBXY_RE_MAX]', 'preprocessing'], \
           ['th_tbxy_Re_max', 50, [-100, 100], 'K', 'Antenna level Re TBXY range check must belong to [TH_TBXY_RE_MIN, TH_TBXY_RE_MAX]', 'preprocessing'], \
           ['th_tbxy_Im_min', -50, [-50, 50], 'K', 'Antenna level Im TBXY range check must belong to [TH_TBXY_IM_MIN, TH_TBXY_IM_MAX]', 'preprocessing'], \
           ['th_tbxy_Im_max', 50, [-50, 50], 'K', 'Antenna level Im TBXY range check must belong to [TH_TBXY_IM_MIN, TH_TBXY_IM_MAX]', 'preprocessing'], \

           ['th_hom_st1', 0.1275, [0, 0.5], '', 'Half stokes test application condition: sufficient homogeneous WA required', 'preprocessing'], \
           ['th_min_angle', 20, [0, 60], '', 'The angle below which the TB will be rejected', 'preprocessing'], \
           ['th_max_angle', 55, [0, 60], '', 'The angle above whcih the TB will be rejected', 'preprocessing'], \
           ['th_min_angle_separation', 10, [0, 60], '', 'The angle separation below which processing will not proceed', 'preprocessing'], \
           ['th_std_tb', 5, [0, 20], '', 'The constant threshold used to reject a TB when comparing std with accuracy', 'preprocessing'], \

           ['dtb_f', 2.57, [2, 8], 'K', 'Scaling factor used to compute MVAL0', 'preprocessing'], \
           ['cval_2', 0.5, [0.4, 1], '', 'Coefficient used to compute MVAL0', 'preprocessing'], \
           ['cval_4', 0.6, [0.4, 1], '', 'Coefficient used to compute MVAL0', 'preprocessing'], \
           ['th_mmin0', 3, [0, 15], '', 'Minimum threshold on the number of available TB after L1c pixel filtering', 'preprocessing'], \
           ['th_ava_min', 20, [], '', 'Minimum number of paired views for applying RFI L2 test', 'preprocessing'], \
           ['ca_tbs1', 5, [], '', 'Coefficient for RFI L2 test', 'preprocessing'], \
           ['cb_tbs1',4, [], '', 'Coefficient for RFI L2 test', 'preprocessing'], \
           ['th_rfi_st4', 50, [0, 300], 'K', 'Threshold for detecting RFI using the 4th Stokes parameter', 'preprocessing'], \

           ['WEF_SIZE', 123, [], 'km', 'Size of the squared fine grid area (in km) over which MEAN_WEF fractions, WEFfractions and reference parameter values are computed', 'wef_meanwef'], \
           ['c_wef1', 73.30, [], '', 'Coefficient in WEF approximation', 'wef'], \
           ['c_wef2', 1.4936, [], '', 'Coefficient in WEF approximation', 'wef'], \
           ['c_wef3', 524.5, [], '', 'Coefficient in WEF approximation', 'wef'], \
           ['c_wef4', 2.1030, [], '', 'Coefficient in WEF approximation', 'wef'], \
           ['c_mwef1', 40,[],'km','Parameter in MEAN_WEF approximation', 'meanwef'], \

           ['c_mwef2',0.0027, [], '', 'Parameter in MEAN_WEF approximation', 'meanwef'], \
           ['tg', 288, [250, 350], 'K', 'Soil effective temperature. Auxiliary ECMWF data fall back default', 'all_model'], \
           ['default_init_sm', 0.2, [0, 1], 'sm^3/sm', 'Default Initial SM value', 'all_model'], \
           ['default_sigma_sm',0.2, [0, 1], 'sm^3/sm', 'Default sigma of SM', 'all_model'], \
           ['default_init_taus_nad', 0.5, [0, 2], '', 'Default initial Tau Nadir', 'all_model'], \
           ['default_sigma_taus_nad',1.0, [0, 2], '', 'Default sigma of Tau Nadir', 'all_model'], \
           ['default_sigma_tbh', 4, [0, 10], 'K', 'Default sigma of TB_H', 'all_model'], \
           ['default_sigma_tbv', 4, [0, 10], 'K', 'Default sigma of TB_V', 'all_model'], \

           ['sym',0, [0, 1], '', 'Symetrization around SM=0 0=No, 1=Yes', 'All Soil dielectic model'], \
           ['tpoly',0, [0, 1, 2, 3], '', 'Symetrization type', 'All Soil dielectic model'], \
           ['sm1',0.02, [0, 0.1], '', 'SM Threshold for symetrizatrion','All Soil dielectic model'], \

           ['rhos', 2.664, [0.5, 3], 'g.cm^-3', 'Soil particle density', 'dobson_model'], \
           ['rhob', 1.3, [0.5, 3], 'g.cm^-3', 'standard soil specific density', 'dobson_model'], \
           ['alpha', 0.65, [], '', 'Dobson model empirical coefficients', 'dobson_model'], \
           ['sal', 35, [0, 50], 'ppt', 'Soil salinity: be kept for future use', 'dobson_model'], \
           ['cpa1', 1.01, [], 'F^1/2.m^-1/2', 'Coefficients for computing dielectric constant of solid particles epsilon_pa', 'dobson_model'], \
           ['cpa2', 0.44, [], 'F^1/2.m.g^-1/2', 'Coefficients for computing dielectric constant of solid particles epsilon_pa', 'dobson_model'], \
           ['cpa3', -0.062, [], 'F.m^-1', 'Coefficients for computing dielectric constant of solid particles epsilon_pa', 'dobson_model'], \
           ['epsilon_pa', 4.7, [], 'Dielectric content  of solid particles', 'F.m^-1', 'dobson_model'], \
           ['sgef1', -1.645, [], '', 'Coefficients for computing  sigma_eff', 'dobson_model'], \
           ['sgef2', 1.939, [], '', 'Coefficients for computing  sigma_eff', 'dobson_model'], \
           ['sgef3', -2.256, [], '', 'Coefficients for computing  sigma_eff', 'dobson_model'], \
           ['sgef4', 1.594, [], '', 'Coefficients for computing  sigma_eff', 'dobson_model'], \
           ['bere1', 1.2748, [], '', 'Coefficients for computing beta_epsilon_prime', 'dobson_model'], \
           ['bere2', -0.519, [], '', 'Coefficients for computing beta_epsilon_prime', 'dobson_model'], \
           ['bere3',-0.152, [], '', 'Coefficients for computing beta_epsilon_prime', 'dobson_model'], \
           ['beim1', 1.338, [], '', 'Coefficients for computing beta_epsilon_second', 'dobson_model'], \
           ['beim2', -0.603, [], '', 'Coefficients for computing beta_epsilon_second', 'dobson_model'], \
           ['beim3', -0.166, [], '', 'Coefficients for computing beta_epsilon_second', 'dobson_model'], \

           ['permit0', 8.854e-12, [], '', '', 'mironov_model'], \
           ['epwi0',4.9, [], '', '', 'mironov_model'], \
           ['nd0', 1.62847, [], '', '', 'mironov_model'], \
           ['nd1', -0.70803, [], '', '', 'mironov_model'], \
           ['nd2', 0.4659, [], '', '', 'mironov_model'], \
           ['kd0', 0.03945, [], '', '', 'mironov_model'], \
           ['kd1', -0.03721, [], '', '', 'mironov_model'], \
           ['xmvt0', 0.00625, [], '', '', 'mironov_model'], \
           ['xmvt1', 0.33918, [], '', '', 'mironov_model'], \
           ['tf0', 20, [], '', '', 'mironov_model'], \
           ['e0pb0', 79.82918, [], '', '', 'mironov_model'], \
           ['e0pb1', -85.36581, [], '', '','mironov_model'], \
           ['e0pb2', 32.70444, [], '', '', 'mironov_model'], \
           ['bvb0', -5.96311e-19, [], '', '', 'mironov_model'], \
           ['bvb1', -1.25999e-3, [], '', '', 'mironov_model'], \
           ['bvb2', 1.83991e-3, [], '', '' , 'mironov_model'], \
           ['bvb3', -9.77347e-4, [], '', '', 'mironov_model'], \
           ['bvb4', -1.39013e-7, [], '', '', 'mironov_model'], \
           ['bsgb0', 0.0028, [], '', '', 'mironov_model'], \
           ['bsgb1', 2.37388e-2, [], '', '', 'mironov_model'], \
           ['bsgb2', -2.93876e-2, [], '', '', 'mironov_model'], \
           ['bsgb3', 3.28954e-2, [], '', '', 'mironov_model'], \
           ['bsgb4', -2.00582e-2, [], '', '', 'mironov_model'], \
           ['dhbr0', 1466.80741, [], '', '', 'mironov_model'], \
           ['dhbr1', 26.97032e2, [], '', '', 'mironov_model'], \
           ['dhbr2', -0.09803e4, [], '', '', 'mironov_model'], \
           ['dsrb0', 0.88775, [], '', '', 'mironov_model'], \
           ['dsrb1', 0.09697e2, [], '', '', 'mironov_model'], \
           ['dsrb2', -4.2622, [], '', '', 'mironov_model'], \
           ['taub0', 48e-12, [], '', '', 'mironov_model'], \
           ['sbt0', 0.29721, [], '', '', 'mironov_model'], \
           ['sbt1', 0.49, [], '', '', 'mironov_model'], \
           ['e0pu', 100, [], '', '', 'mironov_model'], \
           ['bvu0', 1.10511e-4, [], '', '', 'mironov_model'], \
           ['bvu1', -5.16834e-6, [], '', '', 'mironov_model'], \
           ['bsgu0', 0.00277, [], '', '', 'mironov_model'], \
           ['bsgu1', 3.58315e-2, [], '', '', 'mironov_model'], \
           ['dhur0', 2230.20237, [], '', '', 'mironov_model'], \
           ['dhur1', -0.39234e2, [], '', '', 'mironov_model'], \
           ['dsur0', 3.6439, [], '', '', 'mironov_model'], \
           ['dsur1', -0.00134e2, [], '', '', 'mironov_model'], \
           ['tauu0', 48e-12, [], '', '', 'mironov_model'
           ['sut0', 0.12799, [], '', '', 'mironov_model'], \
           ['sut1', 1.65164, [], '', '', 'mironov_model'], \

           ['w0', 0.3, [] , 'm^3/m^3', 'w0 and bw0 are used to obtain the weighting coeff Ct for computing Ts. Those parameters depend mainly on the soil texture and structure. They are superseded by values in the DFFG_SOIL_PROPERTIES table when available', 'effective_temp'], \
           ['bw0', 0.3, [], '', 'w0 and bw0 are used to obtain the weighting coeff Ct for computing Ts. Those parameters depend mainly on the soil texture and structure. They are superseded by values in the DFFG_SOIL_PROPERTIES table when available', 'effective_temp'], \

           ['sst', 288, [260, 320], 'K', 'Water temperature (pure or saline). Fall back default of forecast SST', 'water_model'], \
           ['sss', 35, [0, 40], 'ppt', 'Water salinity (saline water)', 'water_model'], \
           ['ow_1', 88.045, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_2', -0.4147, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_3', 6.295e-4, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_4', 1.075e-5, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_5', 87.134, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_6', -1.949e-1, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_7', -1.276e-2, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_8', 2.491e-4, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_9', 1.0, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_10', 1.613e-5,[], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_11', -3.656e-3, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_12', 3.210e-5, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_13', -4.232e-7, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_14', 1.1109e-10, [], '', 'Stogryn', 'dielectric_water_model'], \
           ['ow_15', -3.824e-12, [], '', 'Stogryn', 'dielectric_water_model'], \
           ['ow_16', 6.938e-14, [], '', 'Stogryn', 'dielectric_water_model'], \
           ['ow_17', -5.096e-16, [], '', 'Stogryn', 'dielectric_water_model'], \
           ['ow_18', 1.0, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_19', 2.282e-5, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_20', -7.638e-4, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_21', -7.760e-6, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_22', 1.105e-8, [], '', 'Klein and Swift', 'dielectric_water_model'], \
           ['ow_23', 0.18252, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_24', -1.4619e-3, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_25', 2.093e-5, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_26', -1.282e-7, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_27', 2.033e-2, [], '', 'Weyl & Stogryn','dielectric_water_model'], \
           ['ow_28', 1.266e-4, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_29', 2.464e-6, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_30', 1.849e-5, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_31', -2.551e-7, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['ow_32', 2.551e-8, [], '', 'Weyl & Stogryn', 'dielectric_water_model'], \
           ['u_card', 0, [0, math.pi], 'rd', 'Angle parameter', 'cardiod_model'], \
           ['b_card', 0.8, [], 'F.m^-1', 'A constant for cardioid model', 'cardiod_model'], \
           ['epsilon_sand', 2.53 - i*0.05, [], 'F.m^-1', 'Dielectric constant for dry sand', 'dielectric_constant_model'], \
           ['epsilon_frz', 5 - i*0.5, [], 'F.m^-1', 'Dielectric constant for frozen soil', 'dielectric_constant_model'], \
           ['epsilon_ice', 3.17 - i*0.1, [], 'F.m^-1', 'Dielectric constant for ice. epsi" is very small for pure ice Currently suggested eps_ice"= 0.1','dielectric_constant_model'], \
           ['epsilon_urban', 5.7 - i*0.074, [], 'F.m^-1', 'Dielectric constant for urban area', 'dielectric_constant_model'], \
           ['epsilon_rock', 5.7 - i*0.074, [2.4 + i*0.074, 9.6 + i*0.074], 'F.m^-1', 'Dielectric constant for barren areas', 'dielectric_constant_model'], \
           ['epsilon_snow', 1.5 - i*0.026, [1 - i*0.006, 3.4 - i*0.08], 'F.m^-1', 'It will be required by computing the TB contribution from Snow fraction', 'snow_model'], \
          ]
