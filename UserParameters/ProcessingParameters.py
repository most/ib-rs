# -*- coding: utf-8 -*-

import numpy as np


class ProcessingParameters:
    def __init__(self, lccParam, upf):
        self.lc = 12 # the code for FNO
        # the IGBP class codes are {0, 1, .., 16}. The lccParam also
        self.alc = lccParam[self.lc, 0]
        self.hr = lccParam[self.lc, 1]
        self.hr_min = lccParam[self.lc, 2]
        self.qr = lccParam[self.lc, 3]
        self.nrh = lccParam[self.lc, 4]
        self.nrv = lccParam[self.lc, 5]
        self.cl = lccParam[self.lc, 6]
        self.bsl = lccParam[self.lc, 7]
        self.a_l = lccParam[self.lc, 8]
        self.b_l = lccParam[self.lc, 9]
        self.bprime = lccParam[self.lc, 10]
        self.bdprime = lccParam[self.lc, 11]
        self.omega_h = lccParam[self.lc, 12]
        self.diff_omega = lccParam[self.lc, 13]
        self.omega_v = self.omega_h + self.diff_omega # see Rev OO.Od
        self.tt_h = lccParam[self.lc, 14]
        self.rtt = lccParam[self.lc, 15]
        self.tt_v = self.tt_h * self.rtt # see Rev 00.0d
        self.b_t = lccParam[self.lc, 16]
        self.dlcc = lccParam[self.lc, 17]

        # Default soil (some of these parameters may not be needed)
        self.s = upf.sand_frac[0]        # sand fraction
        self.c = upf.clay_frac[0]        # clay fraction
        self.ssal = upf.soil_salinity[0] # Soil salinty when used
        self.xmvt = upf.xmvt[0]          # HR(SM) parameter
        self.fc = upf.fc[0]              # HR(SM) field capacity parameter

        self.w0 = upf.w0                 # texture parameter for effective soil temp.
        self.bw0 = upf.bw0               # texture parameter for effective soil temp.

        # for the time being set global parameters; we may need to set these locally or according to ECMWF
        self.p0 = upf.p0[0]
        self.wvc = upf.wvc[0]

        # for the time being set global objameters; we may need to set these locally or according to ECMWF
        self.rhos = upf.rhos[0]           # standard soil bulk density g.cm^-3
        self.rhob = upf.rhob[0]           # standard soil specific density g.cm^-3

        # ATMOSPHERE
        self.tb_sky_h = upf.tb_sky_h[0]   # deep sky TB. BEFORE 3.7
        self.tb_sky_v = upf.tb_sky_v[0]   # BEFORE 3.7
        self.tau_atm = upf.tau_atm[0]
        self.tb_atm = upf.tb_atm[0]

        self.sm = upf.default_init_sm[0]             # for now, but may change
        self.sigma_sm = upf.default_sigma_sm[0]

        self.taus_nad = upf.default_init_taus_nad[0] # something more in accordance with climatology is better
        self.sigma_taus_nad = upf.default_sigma_taus_nad[0]
        self.window_size = upf.default_window_size[0]

        self.sigma_tbh = upf.default_sigma_tbh[0]
        self.sigma_tbv = upf.default_sigma_tbv[0]

        self.model = 'FNO'              # Use Nominal
        self.model_subtype = 'MIRO'     # et une constante dielecrique utisant le modèle de Mironov

        self.tg_k = None

    # set parameters varying with (i, j) cell
    def setProcessingPar(self, lcc, ecmwf):
        # Standard soil
        self.omega_h = lcc.albedo
        self.omega_v = self.omega_h + self.diff_omega

        self.hr = lcc.roughness
        self.hr_min = self.hr

        self.nrh = lcc.nrh
        self.nrv = lcc.nrv

        self.c = lcc.clayFraction # clay
        self.t_soil_surf = ecmwf.Soil_Temperature_L1
        self.t_soil_deep = ecmwf.Soil_Temperature_L3

        # vegetation Temperature
        # tc_k = par.t_soil_surf # Canopy temp (required)
        self.tc_k = ecmwf.Skin_Temperature
        self.t0_k = ecmwf.Air_Temperature_2m

        exitFlag = 0
        # clay fraction is nan over Antarctica, and hence many pixels get discarded
        if np.isnan(self.t_soil_deep) or np.isnan(self.t0_k) or np.isnan(self.hr) or np.isnan(self.c) or np.isnan(self.omega_h):
            exitFlag = 1

        return exitFlag
